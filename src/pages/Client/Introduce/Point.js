import React from "react";

export default function Point() {
  return (
    <div className="grid grid-cols-5 my-48">
      <div className="col-start-2 col-span-3">
        <div className="text-center my-4 uppercase font-bold text-2xl">
          chương trình tích điểm để được giảm giá
        </div>
        <div className="flex justify-center mb-4">
          <img className="w-1/2" alt="" src="../img/tich_diem.jpg" />
        </div>
        <ul className="text-lg">
          <li className="indent-8">
            Nội dung: Giảm 5% giá trị đơn hàng khi khách hàng đăng ký tài khoản.
            Sau mỗi đơn hàng, khách hàng sẽ được tích điểm. Ví dụ: đơn hàng có
            giá trị 1tr thì điểm khách hàng tích được là 100 điểm. Đến một mốc
            quy định khách hàng sẽ nhận được giảm giá ưu đãi hơn.
          </li>
          <li>
            - Khách hàng vừa đăng ký tài khoản sẽ ở mức rank: Đồng. Ở đây khách
            hàng sẽ được giảm 0% cho mỗi đơn hàng.
          </li>
          <li>
            - Khách hàng khi tích điểm lên đến 1000 điểm thì sẽ lên mức rank:
            Bạc. Ở đây khách hàng sẽ được giảm 5% cho mỗi đơn hàng.
          </li>
          <li>
            - Khách hàng khi tích điểm lên đến 1500 điểm thì sẽ lên mức rank:
            Vàng. Ở đây khách hàng sẽ được giảm 8% cho mỗi đơn hàng.
          </li>
          <li>
            - Khách hàng khi tích điểm lên đến 2500 điểm thì sẽ lên mức rank:
            Bạch kim. Ở đây khách hàng sẽ được giảm 10% cho mỗi đơn hàng.
          </li>
          <li>
            - Khách hàng khi tích điểm lên đến 4000 điểm thì sẽ lên mức rank:
            Kim cương. Ở đây khách hàng sẽ được giảm 15% cho mỗi đơn hàng.
          </li>
          <li>
            Chương trình tích điểm dựa theo số điện thoại của khách hàng sau khi
            hoàn thành các đơn hàng, không phân biệt người mua hàng, chỉ cần
            đúng số điện thoại là được.
          </li>
        </ul>
      </div>
    </div>
  );
}
