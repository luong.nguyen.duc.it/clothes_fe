import React, { useEffect, useState } from "react";
import BannerSlick from "../../../component/Home/BannerSlick";
import Content1 from "../../../component/Home/Content1";
import ProductHome from "../../../component/Product/ProductHome";
import { useDispatch, useSelector } from "react-redux";
import {
  GetAllProductAction,
  GetProductHot,
} from "../../../redux/Actions/ManageProductAction";

export default function Home(props) {
  const dispatch = useDispatch();

  const { lstProduct } = useSelector((state) => state.ManageProductReducer);
  const { lstProductHot } = useSelector((state) => state.ManageProductReducer);

  useEffect(() => {
    dispatch(GetAllProductAction());
    dispatch(GetProductHot());
  }, []);

  let productNew = lstProduct
    .filter(function (item) {
      return item.hot === true;
    })
    .sort(function (a, b) {
      return new Date(b.createdAt) - new Date(a.createdAt);
    })
    .slice(0, 5);

  let productHot = lstProductHot
    .sort(function (a, b) {
      // Sắp xếp theo thứ tự giảm dần của quantity
      return b.quantity - a.quantity;
    })
    .slice(0, 5);
  const filteredProductHot = lstProduct.filter((lstProducts) => {
    return productHot.some((item) => item.product === lstProducts._id);
  });

  let productRead = lstProduct
    .filter(function (item) {
      return item.read === true;
    })
    .sort(function (a, b) {
      return new Date(b.updatedAt) - new Date(a.updatedAt);
    })
    .slice(0, 5);

  return (
    <div>
      <BannerSlick />
      <div className="grid grid-cols-12">
        <div className="col-start-3 col-span-8">
          <Content1 />
          <div className="mt-24">
            <ProductHome product={productNew} title="Sản phẩm nổi bật" />
          </div>
        </div>
      </div>
      <div className="grid grid-cols-12">
        <div className="col-start-3 col-span-8 mt-10">
          <ProductHome product={filteredProductHot} title="Hot Sale" />
        </div>
        <div className="col-start-3 col-span-8 my-10">
          <ProductHome product={productRead} title="Sản phẩm đã xem" />
        </div>
      </div>
    </div>
  );
}
