import React, { Fragment, useEffect, useState } from "react";
import { ADD_CART } from "./../../../redux/Types/ManageCartType";
import { message, Rate, Tabs, Avatar } from "antd";
import {
  AiOutlineMinus,
  AiOutlinePlus,
  AiOutlineShoppingCart,
  AiOutlineDoubleRight,
  AiOutlineRight,
  AiFillPushpin,
} from "react-icons/ai";
import { UserOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import {
  GetAllProductAction,
  GetDetailProductAction,
} from "./../../../redux/Actions/ManageProductAction";
import { IMG } from "../../../utils/Setting/config";
import OneProduct from "../../../component/Product/OneProduct";
import { customPriceDetail } from "../../../utils/Utils/CustomPrice";
import {
  GetAllReviewAction,
  GetStarReviewAction,
} from "./../../../redux/Actions/ManageReviewAction";
import { NavLink } from "react-router-dom/cjs/react-router-dom.min";
import { _categories, _detail } from "../../../utils/Utils/ConfigPath";

export default function ProductDetail(props) {
  let { id } = props.match.params;
  const dispatch = useDispatch();
  // useEffect(() => {
  //     window.location.reload();
  // }, [id])

  const { detailProduct } = useSelector((state) => state.ManageProductReducer);
  const { dataStar } = useSelector((state) => state.ManageReviewReducer);
  const { lstReview } = useSelector((state) => state.ManageReviewReducer);

  const [number, setNumber] = useState(1);

  const { lstProduct } = useSelector((state) => state.ManageProductReducer);

  const renderProduct = () => {
    const randomProducts = lstProduct
      .sort(() => Math.random() - 0.5)
      .slice(0, 4);
    return randomProducts.map((item, index) => {
      return <OneProduct key={index} product={item} />;
    });
  };

  useEffect(() => {
    dispatch(GetStarReviewAction(id));
    dispatch(GetAllReviewAction(id));
  }, []);
  useEffect(() => {
    dispatch(GetDetailProductAction(id));
    dispatch(GetAllProductAction());
  }, []);
  return (
    <Fragment>
      <div className="grid grid-cols-10 mt-5">
        <div className="col-start-3 col-span-6 mb-5 flex items-center text-base font-bold py-2 border-b ">
          <NavLink to="/">
            <div className="text-[#06283d]">Trang chủ</div>
          </NavLink>
          <AiOutlineRight className="mx-2" />
          <NavLink to={_categories}>
            <div className="text-[#06283d]">Sản phẩm</div>
          </NavLink>
          <AiOutlineRight className="mx-2" />
          <NavLink to={`${_detail}/${id}`}>
            <div className="text-[#06283d]">{detailProduct?.product_name}</div>
          </NavLink>
        </div>
        <div className="col-start-3 col-span-6 flex">
          <div className="grid grid-cols-12 ">
            <div className="col-span-5">
              <img src={`${IMG}${detailProduct.product_image}`} alt="" />
            </div>
            <div className="col-start-6 col-span-7 ml-10">
              <div className="grid grid-rows">
                <span className="text-2xl font-bold py-2 uppercase text-[#06283d]">
                  {detailProduct.product_name}
                </span>
              </div>
              <div>
                <Rate allowHalf value={dataStar?.star} />
              </div>
              <div className="py-4 flex">
                <span className="text-4xl font-bold text-[#06283d]">
                  {customPriceDetail(detailProduct).toLocaleString()}{" "}
                  <span className="underline">đ</span>
                </span>
                <span className="text-xl font-medium text-gray-600 line-through py-2 px-4">
                  {(detailProduct.price * 1).toLocaleString()}{" "}
                  <span className="underline">đ</span>
                </span>
              </div>
              <div className="text-base border-b pb-2">
                Tình trạng:{" "}
                {detailProduct?.quantity > 0 ? (
                  <span className="font-bold text-lg text-[#06283d]">
                    Còn hàng
                  </span>
                ) : (
                  <span className="font-bold text-lg text-[#06283d]">
                    Hết hàng
                  </span>
                )}
              </div>
              <div className="my-4 text-lg pb-2 border-b">
                Thông tin sản phẩm:
                {detailProduct.Description}
              </div>

              {detailProduct?.color ? (
                <div>
                  <div className="mb-2 text-base">Màu sắc: </div>
                  <div>
                    <button className="border px-3 mx-1 text-lg font-medium bg-[#06283d] text-white">
                      Đen
                    </button>
                    <button className="border border-[#06283d] text-[#06283d] px-3 mx-1 text-lg font-medium">
                      Trắng
                    </button>
                    <button className="border border-[#06283d] text-[#06283d] px-3 mx-1 text-lg font-medium">
                      Be
                    </button>
                    <button className="border border-[#06283d] text-[#06283d] px-3 mx-1 text-lg font-medium">
                      Nâu
                    </button>
                    <button className="border border-[#06283d] text-[#06283d] px-3 mx-1 text-lg font-medium">
                      Kem
                    </button>
                  </div>
                </div>
              ) : null}

              <div className="my-3">
                <div className="mb-2 text-base">Kích thước</div>
                <div>
                  <button className="border px-3 mx-1 text-lg font-medium bg-[#06283d] text-white">
                    M
                  </button>
                  <button className="border border-[#06283d] text-[#06283d] px-3 mx-1 text-lg font-medium">
                    L
                  </button>
                  <button className="border border-[#06283d] text-[#06283d] px-3 mx-1 text-lg font-medium">
                    XL
                  </button>
                  <button className="border border-[#06283d] text-[#06283d] px-3 mx-1 text-lg font-medium">
                    2XL
                  </button>
                </div>
              </div>
              <div className="text-base my-4">
                Số lượng có sãn:{" "}
                <span className="text-[#06283d] font-bold">
                  {detailProduct?.quantity}
                </span>{" "}
                sản phẩm
              </div>

              <div className="flex items-center w-full border-b pb-4">
                <div className="font-bold mr-3">Số lượng: </div>
                <div className="h-10 flex ">
                  <button
                    id="decrease"
                    type="button"
                    onClick={() => {
                      if (number <= 1) {
                        setNumber(1);
                      } else {
                        setNumber(number - 1);
                      }
                    }}
                    className={`border-2 text-lg px-1 ${
                      number === 1
                        ? "inactive cursor-not-allowed"
                        : " hover:text-[#06283d] hover:border-[#06283d]"
                    }`}
                  >
                    <AiOutlineMinus />
                  </button>
                  <input
                    type="text"
                    disabled
                    id="soluong"
                    value={number}
                    name="number"
                    className="text-center w-1/4 p-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-[#06283d] focus:ring-[#06283d] block sm:text-sm focus:ring-1"
                  />
                  <button
                    type="button"
                    onClick={() => {
                      setNumber(number + 1);
                    }}
                    className="border-2 text-lg px-1 hover:text-[#06283d] hover:border-[#06283d]"
                  >
                    <AiOutlinePlus />
                  </button>
                </div>
              </div>
              <div className="my-4">
                <button
                  type="button"
                  onClick={() => {
                    dispatch({
                      type: ADD_CART,
                      data: {
                        item: detailProduct,
                        number,
                      },
                    });
                    message.success("Sản phẩm đã được thêm vào giỏ hàng");
                  }}
                  className="uppercase flex border-2 p-2 text-white bg-black rounded-md text-xl hover:bg-teal-500"
                >
                  Mua hàng <AiOutlineShoppingCart className="mt-2 mx-2 mb-1" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="grid grid-cols-10 mb-20">
        <div className="col-start-3 col-span-6 border-t pt-4">
          <div>
            <Tabs defaultActiveKey="1">
              <Tabs.TabPane tab="MÔ TẢ" key="1">
                <div className="font-bold text-[#06283d]">
                  Chi tiết áo sơ mi Roway
                </div>
                <pre className="mb-0">
                  - Chất liệu: {detailProduct?.material}
                </pre>
                <pre>{detailProduct?.description}</pre>
                <div className="font-bold text-[#06283d] mt-10">
                  Cách chọn size: Bạn NÊN INBOX, cung cấp chiều cao, cân nặng để
                  shop tư vấn size chuẩn nhất
                </div>
                <div className="font-bold text-[#06283d]">
                  - Bảng size mẫu Roway
                </div>
                <img src="/img/bangSize.jpg" alt="" />
                <div className="font-bold text-[#06283d] mb-10">
                  Sản phẩm được đóng 2 lớp hộp chống móp méo khi vận chuyển
                </div>
                <div className="font-bold text-[#06283d]">
                  Là khách hàng của Roway, chúng tôi cam kết bạn sẽ được
                </div>
                <div>
                  1. Sản phẩm giống mô tả và hình ảnh thật 100% của Shop giữ bản
                  quyền hình ảnh.
                </div>
                <div>2. Đảm bảo vải chất lượng sản phẩm 10%%.</div>
                <div>3. Cam kết được đổi trả hàng trong vòng 30 ngày.</div>
                <div>4. Hoàn tiền nếu sản phẩm không giống với mô tả</div>
                <div>
                  + Hàng phải còn mới đầy đủ tem mác và chưa qua sử dụng.
                </div>
                <div className="mb-10">
                  + Sản phẩm bị lỗi do vận chuyển và do nhà sản xuất.
                </div>
                <div className="italic font-bold text-[#06283d] ">
                  Hướng dẫn sử dụng sản phẩm ÁO Roway
                </div>
                <div>- Giặt ở nhiệt độ bình thương.</div>
                <div className="mb-10">- Không được dùng hóa chất tẩy.</div>
                <div className="mb-10">
                  Do màn hình và điều kiện sáng khác nhau, màu sắc thực tế của
                  sản phẩm có thể chênh lệch khoảng 3-5%.
                </div>
                <div className="flex items-center">
                  <AiFillPushpin className="text-red-500" />
                  LƯU Ý: Khi quý khách có gặp bất kì vấn đề gì về sản phẩm và
                  vận chuyện đừng vội đánh giá mà hãy liên hệ Shop để được hỗ
                  trợ 1 cách tốt nhất nhé.
                </div>
              </Tabs.TabPane>
              <Tabs.TabPane tab="CHÍNH SÁCH BẢO HÀNH" key="2">
                <div className="my-2">
                  Những sản phẩm được đổi khi đảm bảo các điều kiện sau:
                </div>
                <div className="my-2">
                  - Thời gian đổi sản phẩm trong vòng 03 ngày (kể từ ngày xuất
                  bán đối với mua trực tiếp hoặc kể từ ngày nhận được sản phẩm
                  đối với mua hàng qua mạng).
                </div>
                <div className="my-2">
                  - Sản phẩm phải nguyên vẹn, còn nguyên tem mác như khi shop
                  bàn giao.
                </div>
                <div className="my-2">
                  - Sản phẩm đổi phải ngang giá hoặc lớn hơn so với sản phẩm
                  trả.
                </div>
                <div className="my-2">
                  - Tất cả các chi phí phát sinh khi đổi trả khách hàng phải
                  chịu 100%.
                </div>
                <div className="my-2">
                  Những trường hợp được bảo hành, sửa chữa không tính phí:
                </div>
                <div className="my-2">
                  - Đối với các sản phẩm quần âu, áo sơ mi do shop sản xuất,
                  khách hàng có nhu cầu lên gấu, bóp eo chỉnh sửa form dáng áo,
                  quần,...
                </div>
                <div className="my-2">
                  - Đối với sản phẩm giày da, thắt lương shop hiện không nhận
                  bảo hành.
                </div>
                <div className="my-2">
                  Những trường hợp bảo hành có tính phí:
                </div>
                <div className="my-2">
                  - Đối với các sản phẩm quần, áo mà khách hàng trong quá trình
                  sử dụng bị rách, hỏng và có nhu cầu sửa chữa, cũng như shop có
                  khả năng sửa chữa được.
                </div>
              </Tabs.TabPane>
            </Tabs>
          </div>
          <div className="uppercase mt-20 font-bold text-2xl text-[#06283d] border-b pb-2">
            Đánh giá sản phẩm
          </div>
          <div>
            {lstReview?.map((item, index) => {
              return (
                <div className="my-4">
                  <div className="flex items-center">
                    <Avatar icon={<UserOutlined />} />
                    <div className="font-bold text-[#06283d] ml-2">
                      {item.account.fullname}
                    </div>
                    <div className="ml-2 -mt-2">
                      <Rate allowHalf value={item?.star} />
                    </div>
                  </div>
                  <div className="ml-10">{item.comment}</div>
                  <img
                    className="h-32 w-32"
                    src={`${IMG}${item.review_image}`}
                    alt=""
                  />
                </div>
              );
            })}
          </div>
          <div className="mt-28 border-t pt-4">
            <h1 className="uppercase text-2xl">Các sản phẩm khác</h1>
            <div className="flex text-center">{renderProduct()}</div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
