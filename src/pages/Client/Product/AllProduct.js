import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  GetAllProductAction,
  GetProductByNameAction,
  UpdateCheckReadAction,
} from "../../../redux/Actions/ManageProductAction";
import { AiOutlineRight } from "react-icons/ai";
import {} from "./../../../redux/Reducers/ManageCategoryReducer";
import { NavLink } from "react-router-dom/cjs/react-router-dom.min";
import { _categories, _detail } from "../../../utils/Utils/ConfigPath";
import { Checkbox, Select } from "antd";
import { IMG } from "../../../utils/Setting/config";

export default function AllProduct() {
  const dispatch = useDispatch();
  const valueSearch = sessionStorage.getItem("search");

  const { lstProduct } = useSelector((state) => state.ManageProductReducer);

  useEffect(() => {
    if (valueSearch !== undefined) {
      dispatch(GetProductByNameAction(valueSearch));
    }
  }, [valueSearch]);

  useEffect(() => {
    dispatch(GetAllProductAction());
  }, []);
  const handleCheckRead = (id) => {
    dispatch(UpdateCheckReadAction(id));
  };

  const renderProduct = (lst) => {
    return lst?.map((product, index) => {
      const discountedPrice = Math.floor(
        product.price - (product.price * product.discount) / 100
      );

      const roundedDiscountedPrice =
        discountedPrice % 1000 >= 500
          ? Math.ceil(discountedPrice / 1000) * 1000
          : Math.floor(discountedPrice / 1000) * 1000;
      return (
        <div className="mx-2 mb-4 border shadow-lg" key={index}>
          <div className="relative">
            <NavLink
              to={`${_detail}/${product._id}`}
              title={`${product.product_name}`}
              onClick={() => handleCheckRead(product?._id)}
            >
              <img
                className="hover:scale-105 w-full"
                src={`${IMG}${product.product_image}`}
                alt={`${product.product_name}`}
              />
            </NavLink>

            {product.discount > 0 ? (
              <div className="absolute top-2 left-2 w-10 h-10 flex items-center justify-center rounded-full bg-[#F13112] text-white">
                {product.discount}%
              </div>
            ) : (
              ""
            )}
          </div>

          <NavLink
            to={`${_detail}/${product._id}`}
            title={product.product_name}
            onClick={() => handleCheckRead(product?._id)}
          >
            <h3 className="mx-4 text-[#06283d] hover:text-green-900 mt-2 text-center">
              {product.product_name}
            </h3>
          </NavLink>
          {product.discount > 0 ? (
            <div className="flex justify-around mb-2">
              <div className="line-through">
                {(product.price * 1).toLocaleString()}
                <span className="underline">đ</span>
              </div>
              <div className="font-medium text-red-500">
                {roundedDiscountedPrice.toLocaleString()}
                <span className="underline">đ</span>
              </div>
            </div>
          ) : (
            <div className="font-medium text-red-500">
              {(product.price * 1).toLocaleString()}
              <span className="underline">đ</span>
            </div>
          )}
        </div>
      );
    });
  };

  const onChange = (checkedValues) => {
    console.log("checked = ", checkedValues);
  };

  const options = [
    {
      label: "Dưới 500.000đ",
      value: "Apple",
    },
    {
      label: "Từ 500.000đ - 1.000.000đ",
      value: "Pear",
    },
    {
      label: "Từ 1.000.000đ - 1.500.000đ",
      value: "Orange",
    },
    {
      label: "Trên 1.500.000đ",
      value: "Oranges",
    },
  ];

  return (
    <div className="mt-5">
      <div className="grid grid-cols-5">
        <div className="col-start-2 col-span-3">
          <div className="col-start-3 col-span-6 mb-5 flex items-center text-base font-bold py-2 border-b ">
            <NavLink to="/">
              <div className="text-[#06283d]">Trang chủ</div>
            </NavLink>
            <AiOutlineRight className="mx-2" />
            <NavLink to={`${_categories}`}>
              <div className="text-[#06283d]">Sản phẩm</div>
            </NavLink>
          </div>
        </div>
        <div className="col-start-2 col-span-3">
          <div className="grid grid-cols-10">
            <div className="col-span-2">
              <div className="border mr-2 pb-5">
                <div className="uppercase text-lg mx-5 py-5 border-b">
                  Kích cỡ
                </div>
                <div className="uppercase text-lg mx-5 py-5 border-b ">
                  Màu sắc
                </div>
                <div className="uppercase text-lg mx-5 py-5">Khoảng giá</div>
                <div className="mx-5">
                  <Checkbox.Group
                    style={{ fontSize: "20px" }}
                    options={options}
                    onChange={onChange}
                  />
                </div>
              </div>
            </div>
            <div className="col-span-8 ml-2">
              <div className="flex justify-between">
                <Select
                  className="w-1/4"
                  allowClear
                  placeholder="Sắp xếp theo"
                  optionFilterProp="children"
                  onChange={onChange}
                  filterOption={(input, option) =>
                    (option?.label ?? "")
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                />
                <div>Hiển thị 48/120</div>
                <div className="flex items-center">
                  Trang:
                  <div className="ml-1">
                    <Select
                      defaultValue="1"
                      style={{
                        width: 50,
                      }}
                      options={[
                        {
                          value: "1",
                          label: "1",
                        },
                      ]}
                    />
                    / 1
                  </div>
                </div>
              </div>
              <div className="grid grid-cols-3 mt-2">
                {renderProduct(
                  lstProduct.sort(function (a, b) {
                    return new Date(b.createdAt) - new Date(a.createdAt);
                  })
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
