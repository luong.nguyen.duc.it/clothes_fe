import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GetCheckoutHistory } from "../../../redux/Actions/ManageCheckoutAction";
import { IMG } from "../../../utils/Setting/config";
import moment from "moment";
import { Steps, Modal } from "antd";
import { GetListUserAction } from "../../../redux/Actions/ManageAccountAction";
import { GetAllRankAction } from "../../../redux/Actions/ManageRankAction";
import ReviewProduct from "./ReviewProduct";

export default function OrderHistory() {
  const dispatch = useDispatch();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [checkoutID, setCheckoutID] = useState();
  const [productID, setProductID] = useState();
  const [productName, setProductName] = useState();

  const { cartHistory } = useSelector((state) => state.ManageCartReducer);

  const { userLogin } = useSelector((state) => state.ManageAccountReducer);
  const { lstRank } = useSelector((state) => state.ManageRankReducer);
  useEffect(() => {
    dispatch(GetCheckoutHistory());
    dispatch(GetAllRankAction());
  }, []);
  const { Step } = Steps;
  let discountUser = 0;
  lstRank.map((item, index) => {
    if (item._id === userLogin?.accountLogin?.rank) {
      discountUser = item.discount;
    }
  });
  const customPrice = (item) => {
    const discountedPrice = Math.floor(
      item.Price - item.Price * (item.Discount / 100)
    );

    const roundedDiscountedPrice =
      discountedPrice % 1000 >= 500
        ? Math.ceil(discountedPrice / 1000) * 1000
        : Math.floor(discountedPrice / 1000) * 1000;
    return roundedDiscountedPrice;
  };

  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const renderTable = () => {
    return cartHistory
      ?.sort(function (a, b) {
        return new Date(b.updatedAt) - new Date(a.updatedAt);
      })
      .map((item, index) => {
        let stepIndex = 0;
        if (item.status_done) {
          stepIndex = 4;
        } else if (!item.status_done && item.status_delivery) {
          stepIndex = 2;
        } else if (
          !item.status_done &&
          !item.status_delivery &&
          !item.StatusAwait
        ) {
          stepIndex = 1;
        } else {
          stepIndex = 0;
        }
        const resultTotal =
          1 * item.total_money - (1 * item.total_money * discountUser) / 100;
        const resultTotalDiscountedPrice =
          resultTotal % 1000 >= 500
            ? Math.ceil(resultTotal / 1000) * 1000
            : Math.floor(resultTotal / 1000) * 1000;

        return (
          <div
            className="p-4 border-2 rounded-lg border-[#06283d] shadow-2xl my-8"
            key={index}
          >
            <div className="mb-4">
              <p className="text-center font-bold text-2xl text-blue-500">
                Trạng thái đơn hàng
              </p>
              <Steps current={stepIndex}>
                <Step
                  style={{ lineHeight: "initial" }}
                  title="Đặt hàng thành công"
                />
                <Step title="Chuẩn bị hàng" />
                <Step title="Đang giao" />
                <Step title="Giao thành công" />
              </Steps>
            </div>
            <span className="text-lg my-2">
              <span className="font-bold">Hóa đơn:</span>{" "}
              <span className="text-[#06283d]">{item._id}</span> -{" "}
              <span className="font-bold">Ngày thanh toán :</span>{" "}
              <span className="text-[#06283d]">
                {moment(item.createdAt).format("DD/MM/YYYY")}
              </span>
            </span>
            {item.checkout_detail.map((detail, indexD) => {
              const result =
                customPrice(detail) -
                (customPrice(detail) * discountUser) / 100;
              const roundedDiscountedPrice =
                result % 1000 >= 500
                  ? Math.ceil(result / 1000) * 1000
                  : Math.floor(result / 1000) * 1000;

              return (
                <div
                  key={indexD}
                  className="flex justify-between border-b-2 p-3 items-center my-2"
                >
                  <div className="w-36">
                    <img src={`${IMG}${detail.ProductImage}`} alt="123" />
                  </div>
                  <p>{detail.ProductName}</p>
                  <div className="flex">
                    <p>
                      {(detail.Price * 1).toLocaleString()}{" "}
                      <span className="underline mr-4">đ</span>
                    </p>
                    <span>x{detail.Quantity}</span>
                  </div>
                  <p>Giảm giá: {detail.Discount}%</p>
                  <p>Giảm giá tài khoản: {discountUser}%</p>
                  <p className="text-base font-medium text-red-500">
                    {roundedDiscountedPrice.toLocaleString()}{" "}
                    <span className="underline mr-4">đ</span>
                  </p>
                  {item.status_done ? (
                    <p>
                      <button
                        className="border px-4 py-2 hover:bg-[#06283d] hover:text-white"
                        onClick={() => {
                          showModal();
                          setCheckoutID(item._id);
                          setProductID(detail.Product_ID);
                          setProductName(detail.ProductName);
                        }}
                      >
                        Đánh giá
                      </button>
                    </p>
                  ) : null}
                </div>
              );
            })}
            <div className="flex justify-end text-right py-2">
              <div>
                <span className="text-2xl">
                  Tổng tiền:{" "}
                  <span className="font-bold text-red-500">
                    {resultTotalDiscountedPrice.toLocaleString()}
                    <span className="underline mr-4">đ</span>
                  </span>
                </span>
              </div>
            </div>
          </div>
        );
      });
  };
  return (
    <Fragment>
      <div className="grid grid-cols-5 mt-20 mb-16">
        <div className="col-start-2 col-span-3 mt-4">
          <h2 className="text-4xl font-bold text-center text-[#06283d]">
            Lịch sử đặt hàng
          </h2>
          {renderTable()}
        </div>
      </div>
      <Modal
        title={productName}
        open={isModalOpen}
        onCancel={handleCancel}
        footer={null}
      >
        <ReviewProduct
          accountID={userLogin?.accountLogin?._id}
          productID={productID}
          checkoutID={checkoutID}
          cancel={handleCancel}
        />
      </Modal>
    </Fragment>
  );
}
