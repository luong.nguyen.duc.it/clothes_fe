import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Rate } from "antd";
import { AddReviewAction } from "../../../redux/Actions/ManageReviewAction";

export default function ReviewProduct(props) {
  const { accountID, checkoutID, productID, cancel } = props;
  const dispatch = useDispatch();

  const [star, setStar] = useState();
  console.log("🚀 ~ ReviewProduct ~ star:", star);

  const [img, setImg] = useState("");
  const handleChangeFile = (e) => {
    let file = e.target.files[0];
    if (
      file.type === "image/jpeg" ||
      file.type === "image/jpeg" ||
      file.type === "image/jpg" ||
      file.type === "image/png"
    ) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        setImg(e.target.result); //Hinh base 64
      };
      formik.setFieldValue("review", file);
    }
  };
  const onCancel = () => cancel;

  const formik = useFormik({
    initialValues: {
      account_id: accountID,
      checkout_id: checkoutID,
      product_id: productID,
      star: 0,
      comment: "",
      review: {},
    },
    validationSchema: Yup.object({
      comment: Yup.string().required("Không được trống !"),
    }),
    onSubmit: (values) => {
      let dataReview = new FormData();
      for (let key in values) {
        if (key !== "review") {
          dataReview.append(key, values[key]);
        } else {
          dataReview.append("review", values.review, values.review.name);
        }
      }
      dispatch(AddReviewAction(dataReview));
      onCancel();
    },
  });
  return (
    <div className="p-4">
      <form onSubmit={formik.handleSubmit}>
        <div className="my-4">
          <span className="mr-2">Hình ảnh:</span>
          <input
            name="review"
            type="file"
            onChange={handleChangeFile}
            accept="image/jpeg, image/jpg, image/png"
          />
        </div>
        <div className="my-4">
          <img className="w-36 h-36 rounded-md" src={img} alt="..." />
        </div>
        <div className="mb-2 flex items-center">
          <span className="mr-2">Đánh giá:</span>{" "}
          <Rate
            name="star"
            // value={formik.values.star}
            allowHalf
            onChange={(value) => formik.setFieldValue("star", value)}
          />
        </div>

        <div className="mb-2">Bình luận:</div>
        <input
          type="text"
          name="comment"
          //   value={formik.values.comment}
          onChange={formik.handleChange}
          className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
          placeholder="Tên loại sản phẩm..."
        />
        {formik.errors.comment && formik.touched.comment && (
          <p className="m-0 mt-1 text-red-600">{formik.errors.comment}</p>
        )}
        <div className="text-end mt-16">
          <button
            type="submit"
            className="border-2 border-[#06283d] rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
          >
            Thêm{" "}
          </button>
        </div>
      </form>
    </div>
  );
}
