import { useFormik } from "formik";
import * as Yup from "yup";
import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { UpdateAccountAction } from "../../../redux/Actions/ManageAccountAction";

export default function UpdateAccount() {
  const dispatch = useDispatch();

  const { userLogin } = useSelector((state) => state.ManageAccountReducer);
  console.log("first", userLogin);
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      email: userLogin.accountLogin?.email,
      fullname: userLogin.accountLogin?.fullname,
      password: "",
      phonenumber: userLogin.accountLogin?.phonenumber,
      address: userLogin.accountLogin?.address,
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Email không đúng!"),
      fullname: Yup.string().required("Không được trống !"),
      password: Yup.string()
        .min(6, "Tối thiểu 6 kí tự")
        .required("Không được trống !"),

      phonenumber: Yup.string()
        .matches(/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/, {
          message: "Số điện thoại chưa đúng",
          excludeEmptyString: false,
        })
        .required("Không được trống !"),
      address: Yup.string().required("Không được trống !"),
    }),
    onSubmit: (values) => {
      dispatch(UpdateAccountAction(userLogin.accountLogin._id, values));
    },
  });
  return (
    <div className="grid grid-cols-12 mt-20">
      <div className="col-start-3 col-span-9">
        <div className="grid grid-rows mx-8">
          <span className="uppercase text-2xl text-[#06283d] font-bold font-serif">
            Thông tin tài khoản
          </span>
          <div className="p-4 w-3/4">
            <form onSubmit={formik.handleSubmit}>
              <div className="mb-2">Email:</div>
              <input
                type="text"
                name="email"
                onChange={formik.handleChange}
                value={formik.values?.email}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
              />
              {formik.errors.email && formik.touched.email && (
                <p className="m-0 mt-1 text-red-600">{formik.errors.email}</p>
              )}
              <div className="mt-4 mb-2">Số điện thoại:</div>
              <input
                type="text"
                name="phonenumber"
                onChange={formik.handleChange}
                value={formik.values?.phonenumber}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập số điện thoại..."
              />
              {formik.errors.phonenumber && formik.touched.phonenumber && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.phonenumber}
                </p>
              )}
              <div className="mb-2">Họ và Tên:</div>
              <input
                type="text"
                name="fullname"
                onChange={formik.handleChange}
                value={formik.values?.fullname}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
              />
              {formik.errors.fullname && formik.touched.fullname && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.fullname}
                </p>
              )}
              <div className="mt-4 mb-2">Địa chỉ:</div>
              <input
                type="text"
                name="address"
                onChange={formik.handleChange}
                value={formik.values?.address}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập địa chỉ..."
              />
              {formik.errors.address && formik.touched.address && (
                <p className="m-0 mt-1 text-red-600">{formik.errors.address}</p>
              )}
              <h1 className="mt-4 uppercase text-2xl font-bold">
                Thay đổi mật khẩu
              </h1>
              <span className="mt-4 italic text-base">
                Để đảm bảo tính bảo mật vui lòng đặt mật khẩu trên 6 ký tự!
              </span>
              <div className="mt-4 mb-2">Mật khẩu mới:</div>
              <input
                type="text"
                name="password"
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập mật khẩu mới..."
              />
              {formik.errors.password && formik.touched.password && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.password}
                </p>
              )}
              <div className="text-end mt-16">
                <button
                  type="submit"
                  className="px-4 py-2 border rounded text-lg font-bold hover:text-white hover:bg-[#06283d]"
                >
                  Cập nhật
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
