import { useFormik } from "formik";
import * as Yup from "yup";
import React, { Fragment } from "react";
import { BsBackspace } from "react-icons/bs";
import { useDispatch } from "react-redux";
import { history } from "../../../App";
import { _admin, _role } from "../../../utils/Utils/ConfigPath";
import { AddRoleAction } from "../../../redux/Actions/ManageRoleAction";

export default function CreateRole() {
  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      role_name: "",
      role_type: "",
    },
    validationSchema: Yup.object({
      role_name: Yup.string().required("Không được trống !"),
      role_type: Yup.string().required("Không được trống !"),
    }),
    onSubmit: (values) => {
      dispatch(AddRoleAction(values));
    },
  });
  return (
    <Fragment>
      <div className="grid grid-cols-7">
        <div>
          <button
            type="button"
            title="Trở về trang loại sản phẩm"
            className="text-4xl text-[#06283d] hover:text-green-900"
            onClick={() => {
              history.push(`${_admin}${_role}`);
            }}
          >
            <BsBackspace />
          </button>
        </div>
        <div className="col-span-3 col-start-3 mt-32 rounded-lg shadow-2xl bg-white p-4">
          <h1 className="text-center text-4xl font-bold text-[#06283d]">
            Thêm phân quyền
          </h1>
          <div className="p-4">
            <form onSubmit={formik.handleSubmit}>
              <div className="mb-4">
                <div className="mb-2">Tên cấp phân quyền:</div>
                <input
                  type="text"
                  name="role_name"
                  onChange={formik.handleChange}
                  value={formik.values.role_name}
                  className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                  placeholder="Nhập tên phân quyền..."
                />
                {formik.errors.role_name && formik.touched.role_name && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.role_name}
                  </p>
                )}
              </div>
              <div>
                <div className="mb-2">Loại phân quyền:</div>
                <input
                  type="text"
                  name="role_type"
                  onChange={formik.handleChange}
                  value={formik.values.role_type}
                  className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                  placeholder="Nhập loại phân quyền..."
                />
                {formik.errors.role_type && formik.touched.role_type && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.role_type}
                  </p>
                )}
              </div>

              <div className="text-end mt-16">
                <button
                  type="submit"
                  className="border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
                >
                  Thêm
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
