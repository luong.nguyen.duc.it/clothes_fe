import { Input, Popconfirm, Table } from "antd";
import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../../App";
import {
  _add,
  _admin,
  _cate,
  _edit,
  _rank,
  _role,
} from "../../../utils/Utils/ConfigPath";
import { DeleteCate } from "./../../../redux/Actions/ManageCategoryAction";
import { BsFillTrashFill, BsPencilSquare } from "react-icons/bs";
import { IMG } from "../../../utils/Setting/config";
import {
  DeleteRank,
  GetAllRankAction,
} from "../../../redux/Actions/ManageRankAction";
import {
  DeleteRole,
  GetAllRoleAction,
} from "../../../redux/Actions/ManageRoleAction";

export default function ManageRole() {
  const { Search } = Input;
  const onSearch = (value) => console.log(value);

  const dispatch = useDispatch();

  const { lstRole } = useSelector((state) => state.ManageRoleReducer);

  useEffect(() => {
    dispatch(GetAllRoleAction());
  }, []);
  const cancel = (e) => {
    console.log(e);
  };
  const columns = [
    {
      title: "STT",
      dataIndex: "_id",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Tên",
      dataIndex: "role_name",
    },
    {
      title: "Loại",
      dataIndex: "role_type",
    },

    {
      title: "",
      dataIndex: "id",
      render: (text, role) => {
        return (
          <div className="flex">
            <button
              className="mx-4 text-green-500 hover:text-green-900"
              title="Sửa"
              onClick={() => {
                history.push(`${_admin}${_role}${_edit}/${role._id}`);
              }}
            >
              <BsPencilSquare style={{ fontSize: 25 }} />
            </button>
            <button
              className="mx-4 text-red-500 hover:text-red-900"
              title="Xóa"
            >
              <Popconfirm
                title="Bạn có chắc muốn xóa loại sản phẩm này không?"
                onConfirm={() => {
                  dispatch(DeleteRole(role._id));
                }}
                onCancel={cancel}
                okText="Có"
                cancelText="Không"
              >
                <BsFillTrashFill style={{ fontSize: 25 }} />
              </Popconfirm>
            </button>
          </div>
        );
      },
    },
  ];

  return (
    <Fragment>
      <div className="container mt-4">
        <h2 className="text-4xl font-bold text-center text-[#06283d]">
          Quản lý phân quyền
        </h2>
        <div className="my-10 flex justify-between">
          <button
            type="button"
            className="border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
            onClick={() => {
              history.push(`${_admin}${_role}${_add}`);
            }}
          >
            Thêm
          </button>
          <div className="w-1/2">
            <Search
              size="large"
              placeholder="Bạn muốn tìm gì?..."
              onSearch={onSearch}
              enterButton
            />
          </div>
        </div>
        <Table dataSource={lstRole} columns={columns} rowKey="id" />;
      </div>
    </Fragment>
  );
}
