import { Input, Popconfirm, Table } from "antd";
import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GetAllCateAction } from "../../../redux/Actions/ManageCategoryAction";
import {
  DeleteProductAction,
  GetAllProductAction,
} from "../../../redux/Actions/ManageProductAction";
import { IMG } from "../../../utils/Setting/config";
import { _add, _admin, _edit, _product } from "../../../utils/Utils/ConfigPath";
import { history } from "./../../../App";
import { BsFillTrashFill, BsPencilSquare } from "react-icons/bs";

export default function ManageProduct() {
  const dispatch = useDispatch();

  const { lstProduct } = useSelector((state) => state.ManageProductReducer);
  const { lstCate } = useSelector((state) => state.ManageCategoryReducer);

  useEffect(() => {
    dispatch(GetAllProductAction());
    dispatch(GetAllCateAction());
  }, []);

  const cancel = (e) => {
    console.log(e);
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "_id",
      width: "2%",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Loại sản phẩm",
      dataIndex: "category",
      render: (text, item) => {
        return (
          <Fragment>
            {lstCate?.map((lst, index) => {
              return (
                <span key={index}>
                  {lst._id === item.category ? (
                    <span>{lst.category_name}</span>
                  ) : (
                    ""
                  )}
                </span>
              );
            })}
          </Fragment>
        );
      },
      width: "5%",
    },
    {
      title: "Xuất xứ",
      dataIndex: "origin",
      width: "5%",
    },
    {
      title: "Thương hiệu",
      dataIndex: "brand",
      width: "5%",
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      width: "10%",
    },
    {
      title: "Chất liệu",
      dataIndex: "material",
      width: "10%",
    },
    {
      title: "Hình ảnh",
      dataIndex: "product_image",
      render: (text, item) => {
        return (
          <Fragment>
            <img
              className="h-36 w-36"
              src={`${IMG}${item.product_image}`}
              alt={item.product_image}
            />
          </Fragment>
        );
      },
      width: "12%",
    },

    {
      title: "Mô tả",
      dataIndex: "description",
    },
    {
      title: "Số lượng",
      dataIndex: "quantity",
    },
    {
      title: "Màu sắc",
      dataIndex: "color",
    },
    {
      title: "Kích cỡ",
      dataIndex: "size",
    },
    {
      title: "Giá tiền",
      dataIndex: "price",
      width: "7%",
      render: (text, item) => {
        return (
          <Fragment>
            {(item.price * 1).toLocaleString()}{" "}
            <span className="underline">đ</span>
          </Fragment>
        );
      },
    },
    {
      title: "Giảm giá",
      dataIndex: "discount",
      width: "7%",
      render: (text, item) => {
        return <Fragment>{item.discount} %</Fragment>;
      },
    },
    {
      title: "Hot",
      dataIndex: "hot",
      width: "7%",
      render: (text, item) => {
        return <Fragment>{item.hot === true ? "True" : "False"}</Fragment>;
      },
    },
    {
      title: "",
      dataIndex: "id",
      render: (text, item) => {
        return (
          <div className="flex">
            <button
              className="mx-4 text-green-500 hover:text-green-900"
              title="Sửa"
              onClick={() => {
                history.push(`${_admin}${_product}${_edit}/${item._id}`);
              }}
            >
              <BsPencilSquare style={{ fontSize: 25 }} />
            </button>
            <button
              className="mx-4 text-red-500 hover:text-red-900"
              title="Xóa"
            >
              <Popconfirm
                title="Bạn có chắc muốn xóa sản phẩm không?"
                onConfirm={() => {
                  dispatch(DeleteProductAction(item._id));
                }}
                onCancel={cancel}
                okText="Có"
                cancelText="Không"
              >
                <BsFillTrashFill style={{ fontSize: 25 }} />
              </Popconfirm>
            </button>
          </div>
        );
      },
      width: "5%",
    },
  ];

  const { Search } = Input;
  const onSearch = (value) => {
    dispatch(GetAllProductAction());
  };
  return (
    <Fragment>
      <div className="mt-4">
        <div className="flex justify-center">
          <h2 className="text-4xl font-bold text-[#06283d] flex items-center">
            Quản lý sản phẩm
          </h2>
        </div>
        <div className="my-10 flex justify-between">
          <button
            type="button"
            className="border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
            onClick={() => {
              history.push(`${_admin}${_product}${_add}`);
            }}
          >
            Thêm{" "}
          </button>
          <div className="w-1/3">
            <Search
              size="large"
              placeholder="Bạn muốn tìm gì?..."
              onSearch={onSearch}
              enterButton
            />
          </div>
        </div>
        <Table
          dataSource={lstProduct.sort(function (a, b) {
            return new Date(b.createdAt) - new Date(a.createdAt);
          })}
          columns={columns}
          rowKey="id"
        />
        ;
      </div>
    </Fragment>
  );
}
