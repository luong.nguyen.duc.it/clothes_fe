import React, { useEffect, useState } from "react";
import {
  Chart as ChartJS,
  BarElement,
  CategoryScale,
  LinearScale,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { DatePicker, Space } from "antd";
import moment from "moment";

ChartJS.register(BarElement, CategoryScale, LinearScale, Tooltip, Legend);
export default function Dashboard() {
  const [chartData, setChartData] = useState([]);
  const [year, setYear] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          "http://localhost:8090/api/v1/checkout/Total/Year/Chart",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ year: year }),
          }
        );

        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();
        setChartData(data);
        // Xử lý dữ liệu nếu cần
      } catch (error) {
        console.error("Error fetching data:", error);
        // Xử lý lỗi nếu cần
      }
    };

    fetchData();
  }, [year]);

  const dataByMonth = {
    labels: [
      "Th-01",
      "Th-02",
      "Th-03",
      "Th-04",
      "Th-05",
      "Th-06",
      "Th-07",
      "Th-08",
      "Th-09",
      "Th-10",
      "Th-11",
      "Th-12",
    ],
    datasets: [
      {
        label: "vnđ",
        data: chartData.map((item) => Object.values(item)[0]),
        backgroundColor: "#06283d",
        borderColor: "black",
      },
    ],
  };

  const options = {};
  const onChange = (date, dateString) => {
    setYear(moment(date).format("YYYY"));
  };
  return (
    <div>
      <div className="flex items-center">
        <h1 className="mr-2">Doanh số bán hàng năm:</h1>
        <Space direction="vertical">
          <DatePicker
            placeholder="Chọn năm"
            onChange={onChange}
            picker="year"
          />
        </Space>
      </div>
      <div className="w-full">
        <Bar data={dataByMonth} options={options}></Bar>
      </div>
    </div>
  );
}
