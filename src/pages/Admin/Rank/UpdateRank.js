import { useFormik } from "formik";
import * as Yup from "yup";
import React, { Fragment, useEffect, useState } from "react";
import { BsBackspace } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../../App";
import {
  GetDetailCateAction,
  UpdateCateAction,
} from "../../../redux/Actions/ManageCategoryAction";
import { _admin, _cate, _rank } from "../../../utils/Utils/ConfigPath";
import { IMG } from "../../../utils/Setting/config";
import {
  GetDetailRankAction,
  UpdateRankAction,
} from "../../../redux/Actions/ManageRankAction";

export default function UpdateRank(props) {
  let { id } = props.match.params;
  const dispatch = useDispatch();

  const { detailRank } = useSelector((state) => state.ManageRankReducer);

  useEffect(() => {
    dispatch(GetDetailRankAction(id));
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      rank_name: detailRank?.rank_name,
      point: detailRank?.point,
      discount: detailRank?.discount,
    },
    validationSchema: Yup.object({
      point: Yup.string().required("Không được trống !"),
      discount: Yup.string().required("Không được trống !"),
    }),
    onSubmit: (values) => {
      dispatch(UpdateRankAction(id, values));
    },
  });
  return (
    <Fragment>
      <div className="grid grid-cols-7">
        <div>
          <button
            type="button"
            title="Trở về trang loại sản phẩm"
            className="text-4xl text-[#06283d] hover:text-green-900"
            onClick={() => {
              history.push(`${_admin}${_rank}`);
            }}
          >
            <BsBackspace />
          </button>
        </div>
        <div className="col-span-3 col-start-3 mt-32 h-96 rounded-lg shadow-2xl bg-white p-4">
          <h1 className="text-center text-4xl font-bold text-[#06283d]">
            Cập nhật cấp độ
          </h1>
          <div className="p-4">
            <form onSubmit={formik.handleSubmit}>
              <div className="mb-4">
                <div className="mb-2">Tên cấp độ:</div>
                <input
                  disabled
                  type="text"
                  name="rank_name"
                  onChange={formik.handleChange}
                  value={formik.values.rank_name}
                  className="cursor-not-allowed p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                  placeholder="Nhập tên cấp độ..."
                />
              </div>
              <div className="mb-4">
                <div className="mb-2">Điểm:</div>
                <input
                  type="text"
                  name="point"
                  onChange={formik.handleChange}
                  value={formik.values.point}
                  className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                  placeholder="Nhập điểm..."
                />
                {formik.errors.point && formik.touched.point && (
                  <p className="m-0 mt-1 text-red-600">{formik.errors.point}</p>
                )}
              </div>
              <div className="mb-4">
                <div className="mb-2">Giảm giá:</div>
                <input
                  type="text"
                  name="discount"
                  onChange={formik.handleChange}
                  value={formik.values.discount}
                  className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                  placeholder="Nhập giảm giá..."
                />
                {formik.errors.discount && formik.touched.discount && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.discount}
                  </p>
                )}
              </div>

              <div className="text-end mt-16">
                <button
                  type="submit"
                  className="border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
                >
                  Cập nhật
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
