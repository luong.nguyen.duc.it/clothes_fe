import React, { Fragment, useEffect, useState } from "react";
import { history } from "./../../../App";
import { Input, Popconfirm, Table, Space, DatePicker, Modal } from "antd";
import { AiFillDollarCircle } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import { GetListUserAction } from "../../../redux/Actions/ManageAccountAction";
import locale from "antd/lib/date-picker/locale/vi_VN"; // Import ngôn ngữ tiếng Việt
import "moment/locale/vi"; // Import ngôn ngữ Moment cho tiếng Việt
import moment from "moment";
import { TotalWorkScheduleByAccountAndMonth } from "../../../redux/Actions/ManageWorkScheduleAction";

export default function ManageSalary() {
  const dispatch = useDispatch();
  const { lstUser } = useSelector((state) => state.ManageAccountReducer);
  const { userLogin } = useSelector((state) => state.ManageAccountReducer);
  console.log("🚀 ~ ManageSalary ~ userLogin:", userLogin);
  const { detailTotal } = useSelector(
    (state) => state.ManageWorkScheduleReducer
  );

  const [lstStaff, setLstStaff] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [month, setMonth] = useState();
  const [year, setYear] = useState();
  const [accountID, setAccountID] = useState();
  const [name, setName] = useState();

  moment.locale("vi"); // Đặt ngôn ngữ Moment sang tiếng Việt

  let role = userLogin?.accountLogin.role.role_type;

  useEffect(() => {
    dispatch(GetListUserAction());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    dispatch(TotalWorkScheduleByAccountAndMonth({ Year: year }));
  }, [year]);

  useEffect(() => {
    const filteredStaff = lstUser?.filter(
      (item) => item.role.role_type === "STAFF"
    );

    setLstStaff(filteredStaff || []);
  }, [lstUser]);

  useEffect(() => {
    if (accountID && month && year) {
      const values = {
        Account_ID: accountID,
        Month: month,
        Year: year,
      };
      dispatch(TotalWorkScheduleByAccountAndMonth(values));
    }
  }, [month, accountID]);
  const { Search } = Input;
  const columns = [
    {
      title: "STT",
      dataIndex: "_id",
      width: "2%",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Email",
      dataIndex: "email",
    },
    {
      title: "Họ và Tên",
      dataIndex: "fullname",
    },
    {
      title: "Số điện thoại",
      dataIndex: "phonenumber",
      render: (text, user) => {
        return (
          <Fragment>
            {user.phonenumber === null ? (
              <div className="text-[#06283d] flex text-base italic">
                <span>Chưa có số điện thoại...</span>
              </div>
            ) : (
              <span>{user.phonenumber}</span>
            )}
          </Fragment>
        );
      },
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      render: (text, user) => {
        return (
          <Fragment>
            {user.address === "" ? (
              <div className="text-[#06283d] flex text-base italic">
                <span>Chưa có địa chỉ...</span>
              </div>
            ) : (
              <span>{user.address}</span>
            )}
          </Fragment>
        );
      },
    },
    {
      title: "Tiền lương",
      dataIndex: "id",
      render: (text, item) => {
        return (
          <div className="flex">
            <button
              className="mx-4 text-green-500 hover:text-green-900"
              title="Sửa"
              onClick={() => {
                showModal();
                setName(item.fullname);
                const values = {
                  Account_ID: item?._id,
                  Month: month,
                  Year: year,
                };
                console.log("🚀 ~ useEffect ~ values:", values);
                dispatch(TotalWorkScheduleByAccountAndMonth(values));
              }}
            >
              <AiFillDollarCircle style={{ fontSize: 25 }} />
            </button>
          </div>
        );
      },
      width: "10%",
    },
  ];

  const columnsModel = [
    {
      title: "Tháng",
      dataIndex: "month",
    },
    {
      title: "Tổng giờ làm",
      dataIndex: "total_time",
      render: (text, item) => {
        return (
          <Fragment>
            <span>{item.total_time}h</span>
          </Fragment>
        );
      },
    },
    {
      title: "Mức lương / 1h",
      dataIndex: "salary_level",
      render: (text, item) => {
        const salaryNumber = parseFloat(item?.salary_level);

        return (
          <Fragment>
            {isNaN(salaryNumber) || salaryNumber === 0 ? (
              <div className="text-[#06283d] flex text-base italic">
                <span>Chưa có mức lương...</span>
              </div>
            ) : (
              <span>
                {salaryNumber.toLocaleString("vi-VN", {
                  style: "currency",
                  currency: "VND",
                })}
              </span>
            )}
          </Fragment>
        );
      },
    },
    {
      title: "Tiền lương",
      dataIndex: "salary",
      render: (text, item) => {
        const salaryNumber = parseFloat(item?.salary);

        return (
          <Fragment>
            {isNaN(salaryNumber) || salaryNumber === 0 ? (
              <div className="text-[#06283d] flex text-base italic">
                <span>0 đ</span>
              </div>
            ) : (
              <span>
                {salaryNumber.toLocaleString("vi-VN", {
                  style: "currency",
                  currency: "VND",
                })}
              </span>
            )}
          </Fragment>
        );
      },
    },
  ];

  const onChange = (date, dateString) => {
    setMonth(moment(date._d).format("MM"));
    setYear(moment(date._d).format("YYYY"));
  };
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <Fragment>
      <div className="mt-4">
        <div className="flex justify-center">
          <h2 className="text-4xl font-bold text-[#06283d] flex items-center">
            {role === "ADMIN" ? (
              <span>Quản lý Lương</span>
            ) : (
              <span>Lương của tôi</span>
            )}
          </h2>
        </div>

        {role === "STAFF" ? (
          <>
            <div className="my-10">
              <div>Chọn năm:</div>
              <Space direction="vertical">
                <DatePicker
                  placeholder="Chọn năm"
                  onChange={onChange}
                  picker="year"
                  locale={locale} // Đặt ngôn ngữ cho DatePicker
                  format="YYYY" // Định dạng ngày tháng
                />
              </Space>
            </div>
            <Table
              dataSource={detailTotal}
              columns={columnsModel}
              rowKey="id"
            />
          </>
        ) : (
          <>
            <div className="my-10">
              <div>Chọn tháng:</div>
              <Space direction="vertical">
                <DatePicker
                  placeholder="Chọn tháng"
                  onChange={onChange}
                  picker="month"
                  locale={locale} // Đặt ngôn ngữ cho DatePicker
                  format="MM/YYYY" // Định dạng ngày tháng
                />
              </Space>
            </div>
            <Table dataSource={lstStaff} columns={columns} rowKey="id" />
          </>
        )}
      </div>
      <Modal
        title={name}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Table dataSource={detailTotal} columns={columnsModel} rowKey="id" />;
      </Modal>
    </Fragment>
  );
}
