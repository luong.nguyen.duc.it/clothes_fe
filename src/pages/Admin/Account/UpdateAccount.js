import { useFormik } from "formik";
import * as Yup from "yup";
import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  GetDetailUserAction,
  UpdateUserAction,
} from "../../../redux/Actions/ManageAccountAction";
import { history } from "../../../App";
import { _account, _admin } from "../../../utils/Utils/ConfigPath";
import { BsBackspace } from "react-icons/bs";
import { Select, message } from "antd";
import { Redirect } from "react-router";

export default function AdminUpdateAccount(props) {
  let { id } = props.match.params;

  const dispatch = useDispatch();

  const { editUser } = useSelector((state) => state.ManageAccountReducer);
  const { userLogin } = useSelector((state) => state.ManageAccountReducer);
  let typeLogin = userLogin?.accountLogin?.role?.role_type;
  const uppercaseToTitleCase = (inputText) => {
    if (!inputText) {
      return "";
    }

    const lowercaseText = inputText.toLowerCase();
    const titleCaseText = lowercaseText.replace(/(?:^|\s)\S/g, (match) => {
      return match.toUpperCase();
    });

    return titleCaseText;
  };
  let typeUser = editUser?.role?.role_type;

  useEffect(() => {
    dispatch(GetDetailUserAction(id));
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      email: editUser?.email,
      fullname: editUser?.fullname,
      role: editUser?.role?.role_type,
      password: "",
      phonenumber: editUser?.phonenumber,
      address: editUser?.address,
      salary_level: editUser?.salary_level,
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Email không đúng!"),

      fullname: Yup.string().required("Không được trống !"),

      password: Yup.string().min(6, "Tối thiểu 6 kí tự"),

      phonenumber: Yup.string()
        .matches(/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/, {
          message: "Số điện thoại chưa đúng",
          excludeEmptyString: false,
        })
        .required("Không được trống !"),

      address: Yup.string().required("Không được trống !"),
    }),
    onSubmit: (values) => {
      dispatch(UpdateUserAction(id, values));
    },
  });

  if (typeUser === "CLIENT") {
    message.error("Bạn không có quyền sửa tài khoản này!");
    return <Redirect to={`${_admin}${_account}`} />;
  }
  return (
    <Fragment>
      <div className="grid grid-cols-7 mb-20">
        <div>
          <button
            type="button"
            title="Trở về trang tài khoản"
            className="text-4xl text-[#06283d] hover:text-green-900"
            onClick={() => {
              history.push(`${_admin}${_account}`);
            }}
          >
            <BsBackspace />
          </button>
        </div>
        <div className="col-span-3 col-start-3 mt-16 h-full rounded-lg shadow-2xl bg-white p-4">
          <h1 className="mt-8 text-center text-4xl font-bold text-[#06283d]">
            Cập nhật tài khoản
          </h1>
          <div className="p-4">
            <form onSubmit={formik.handleSubmit}>
              <div className="mb-2">Email:</div>
              <input
                type="text"
                name="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
              />
              {formik.errors.email && formik.touched.email && (
                <p className="m-0 mt-1 text-red-600">{formik.errors.email}</p>
              )}
              <div className="mt-4 mb-2">Họ và tên:</div>
              <input
                type="text"
                name="fullname"
                value={uppercaseToTitleCase(formik.values.fullname)}
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
              />
              {formik.errors.fullname && formik.touched.fullname && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.fullname}
                </p>
              )}
              <div className="mt-4 mb-2">Mật khẩu mới:</div>
              <input
                type="text"
                name="password"
                onChange={formik.handleChange}
                // value={formik.values.password}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập mật khẩu..."
              />
              {formik.errors.password && formik.touched.password && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.password}
                </p>
              )}
              <div className="mt-4 mb-2">Phân quyền:</div>
              <input
                type="text"
                name="role"
                value={formik.values.role}
                className="w-48 p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                disabled
              />

              <div className="mt-4 mb-2">Số điện thoại:</div>
              <input
                type="text"
                name="phonenumber"
                onChange={formik.handleChange}
                value={formik.values.phonenumber}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập số điện thoại..."
              />
              {formik.errors.phonenumber && formik.touched.phonenumber && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.phonenumber}
                </p>
              )}
              <div className="mt-4 mb-2">Địa chỉ:</div>
              <input
                type="text"
                name="address"
                onChange={formik.handleChange}
                value={formik.values.address}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập địa chỉ..."
              />
              {formik.errors.address && formik.touched.address && (
                <p className="m-0 mt-1 text-red-600">{formik.errors.address}</p>
              )}
              {typeUser === "STAFF" ? (
                <>
                  <div className="mt-4 mb-2">Tiền lương / 1h:</div>
                  <input
                    type="text"
                    name="salary_level"
                    onChange={formik.handleChange}
                    value={formik.values.salary_level}
                    className={`p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full `}
                    placeholder="Nhập mức lương..."
                    disabled={typeLogin === "ADMIN" ? false : true}
                  />
                </>
              ) : null}

              <div className="text-end mt-16">
                <button
                  type="submit"
                  className="border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
                >
                  Cập nhật
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
