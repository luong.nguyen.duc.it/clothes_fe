import React, { Fragment, useEffect } from "react";
import { Input, Popconfirm, Table } from "antd";
import { history } from "./../../../App";
import { _account, _add, _admin, _edit } from "../../../utils/Utils/ConfigPath";
import { useDispatch, useSelector } from "react-redux";
import {
  DeleteUserAction,
  GetListUserAction,
} from "../../../redux/Actions/ManageAccountAction";
import { BsFillTrashFill, BsPencilSquare } from "react-icons/bs";
import { GetAllRankAction } from "../../../redux/Actions/ManageRankAction";

const { Search } = Input;

export default function ManageAccount() {
  const dispatch = useDispatch();
  const { lstUser } = useSelector((state) => state.ManageAccountReducer);
  const { userLogin } = useSelector((state) => state.ManageAccountReducer);
  const { lstRank } = useSelector((state) => state.ManageRankReducer);
  console.log("🚀 ~ ManageAccount ~ lstRank:", lstRank);
  let role = userLogin?.accountLogin.role.role_type;
  useEffect(() => {
    dispatch(GetListUserAction());
    dispatch(GetAllRankAction());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSearch = (value) => {
    dispatch(GetListUserAction(value));
  };

  const cancel = (e) => {
    console.log(e);
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "_id",
      width: "2%",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Email",
      dataIndex: "email",
    },
    {
      title: "Họ và Tên",
      dataIndex: "fullname",
    },
    {
      title: "Số điện thoại",
      dataIndex: "phonenumber",
      render: (text, user) => {
        return (
          <Fragment>
            {user.phonenumber === null ? (
              <div className="text-[#06283d] flex text-base italic">
                <span>Chưa có số điện thoại...</span>
              </div>
            ) : (
              <span>{user.phonenumber}</span>
            )}
          </Fragment>
        );
      },
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      render: (text, user) => {
        return (
          <Fragment>
            {user.address === "" ? (
              <div className="text-[#06283d] flex text-base italic">
                <span>Chưa có địa chỉ...</span>
              </div>
            ) : (
              <span>{user.address}</span>
            )}
          </Fragment>
        );
      },
    },
    {
      title: "Quyền",
      dataIndex: "_id",
      render: (text, user) => {
        return (
          <Fragment>
            {user.role.role_type === "CLIENT" ? (
              <span>{user.role.role_type}</span>
            ) : (
              <>
                {user.role.role_type === "STAFF" ? (
                  <span className="text-red-500">{user.role.role_type}</span>
                ) : (
                  <span className="text-red-500 font-bold">
                    {user.role.role_type}
                  </span>
                )}
              </>
            )}
          </Fragment>
        );
      },
    },
    {
      title: "Điểm",
      dataIndex: "_id",
      render: (text, user) => {
        return (
          <Fragment>
            {user.role.role_type === "CLIENT" ? <span>{user.point}</span> : ""}
          </Fragment>
        );
      },
    },
    {
      title: "Xếp hạng",
      dataIndex: "_id",
      render: (text, user) => {
        return (
          <Fragment>
            {user.role.role_type === "CLIENT" ? (
              <span>{lstRank?.map((item,index) => {
                return <>
                {item._id===user.rank ? <span>{item.rank_name}</span>:null}
                </>
              })}</span>
            ) : (
              ""
            )}
          </Fragment>
        );
      },
    },
    {
      title: "",
      dataIndex: "id",
      render: (text, user) => {
        return (
          <>
            {user.role.role_type === "CLIENT" ? (
              <div className="text-center -ml-5">
                <button className="text-red-500 hover:text-red-900" title="Xóa">
                  <Popconfirm
                    title="Bạn có muốn xóa tài khoản không?"
                    onConfirm={() => {
                      dispatch(DeleteUserAction(user.id));
                    }}
                    onCancel={cancel}
                    okText="Có"
                    cancelText="Không"
                  >
                    <BsFillTrashFill style={{ fontSize: 25 }} />
                  </Popconfirm>
                </button>
              </div>
            ) : (
              <>
                {role === "STAFF" ? (
                  <>
                    {user.role.role_type === "ADMIN" ? (
                      <div className="text-center -ml-5">
                        <button
                          className="text-red-500 hover:text-red-900"
                          title="Xóa"
                        >
                          <Popconfirm
                            title="Bạn có muốn xóa tài khoản không?"
                            onConfirm={() => {
                              dispatch(DeleteUserAction(user._id));
                            }}
                            onCancel={cancel}
                            okText="Có"
                            cancelText="Không"
                          >
                            <BsFillTrashFill style={{ fontSize: 25 }} />
                          </Popconfirm>
                        </button>
                      </div>
                    ) : (
                      <div className="flex">
                        <button
                          className="mx-4 text-green-500 hover:text-green-900"
                          title="Sửa"
                          onClick={() => {
                            history.push(
                              `${_admin}${_account}${_edit}/${user._id}`
                            );
                          }}
                        >
                          <BsPencilSquare style={{ fontSize: 25 }} />
                        </button>
                        <button
                          className="mx-4 text-red-500 hover:text-red-900 content-end"
                          title="Xóa"
                        >
                          <Popconfirm
                            title="Bạn có muốn xóa tài khoản không?"
                            onConfirm={() => {
                              dispatch(DeleteUserAction(user._id));
                            }}
                            onCancel={cancel}
                            okText="Có"
                            cancelText="Không"
                          >
                            <BsFillTrashFill style={{ fontSize: 25 }} />
                          </Popconfirm>
                        </button>
                      </div>
                    )}
                  </>
                ) : (
                  <div className="flex">
                    <button
                      className="mx-4 text-green-500 hover:text-green-900"
                      title="Sửa"
                      onClick={() => {
                        history.push(
                          `${_admin}${_account}${_edit}/${user._id}`
                        );
                      }}
                    >
                      <BsPencilSquare style={{ fontSize: 25 }} />
                    </button>
                    <button
                      className="mx-4 text-red-500 hover:text-red-900 content-end"
                      title="Xóa"
                    >
                      <Popconfirm
                        title="Bạn có muốn xóa tài khoản không?"
                        onConfirm={() => {
                          dispatch(DeleteUserAction(user._id));
                        }}
                        onCancel={cancel}
                        okText="Có"
                        cancelText="Không"
                      >
                        <BsFillTrashFill style={{ fontSize: 25 }} />
                      </Popconfirm>
                    </button>
                  </div>
                )}
              </>
            )}
          </>
        );
      },
    },
  ];
  return (
    <Fragment>
      <div className="container mt-4">
        <div className="flex justify-center">
          <h2 className="text-4xl font-bold text-[#06283d]">
            Quản lý Tài khoản
          </h2>
        </div>
        <div className="my-10 flex justify-between">
          {role === "ADMIN" ? (
            <button
              type="button"
              className="border-2 border-[#06283d] rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
              onClick={() => {
                history.push(`${_admin}${_account}${_add}`);
              }}
            >
              Thêm{" "}
            </button>
          ) : (
            <button
              type="button"
              className="border-2 border-[#06283d] rounded w-24 h-10 text-lg font-bold text-[#06283d] cursor-not-allowed"
              title="Bạn không có quyền thực hiện thao tác này!"
              disabled
              onClick={() => {
                history.push(`${_admin}${_account}${_add}`);
              }}
            >
              Thêm{" "}
            </button>
          )}
          <div className="w-1/2">
            <Search
              size="large"
              placeholder="Bạn muốn tìm gì?..."
              onSearch={onSearch}
              enterButton
            />
          </div>
        </div>
        <Table dataSource={lstUser} columns={columns} rowKey="id" />
      </div>
    </Fragment>
  );
}
