import { useFormik } from "formik";
import * as Yup from "yup";
import React, { Fragment } from "react";
import { useDispatch } from "react-redux";
import { AddUserAction } from "../../../redux/Actions/ManageAccountAction";
import { history } from "../../../App";
import { _account, _admin } from "../../../utils/Utils/ConfigPath";
import { BsBackspace } from "react-icons/bs";
import { Select } from "antd";

export default function CreateAccount() {
  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      email: "",
      fullname: "",
      password: "",
      role: "",
      phonenumber: "",
      address: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Email không đúng!"),
      fullname: Yup.string().required("Không được trống !"),

      password: Yup.string()
        .min(6, "Tối thiểu 6 kí tự")
        .required("Không được trống !"),

      role: Yup.string().required("Không được trống !"),

      address: Yup.string().required("Không được trống !"),

      phonenumber: Yup.string()
        .matches(/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/, {
          message: "Số điện thoại chưa đúng",
          excludeEmptyString: false,
        })
        .required("Không được trống !"),
    }),
    onSubmit: (values) => {
      dispatch(AddUserAction(values));
    },
  });
  function changeSelect(value) {
    if (value === "STAFF") {
      formik.setFieldValue("role", "65a0a989a75e8296e13b778e");
    } else {
      formik.setFieldValue("role", "65a0a989a75e8296e13b778f");
    }
  }
  return (
    <Fragment>
      <div className="grid grid-cols-7">
        <div>
          <button
            type="button"
            title="Trở về trang tài khoản"
            className="text-4xl text-[#06283d] hover:text-green-900"
            onClick={() => {
              history.push(`${_admin}${_account}`);
            }}
          >
            <BsBackspace />
          </button>
        </div>
        <div className="col-span-3 col-start-3 mt-16 h-full rounded-lg shadow-2xl bg-white p-4">
          <h1 className="mt-8 text-center text-4xl font-bold text-[#06283d]">
            Thêm tài khoản
          </h1>
          <div className="p-4">
            <form onSubmit={formik.handleSubmit}>
              <div className="mb-2">Email:</div>
              <input
                type="text"
                name="email"
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập email..."
              />
              {formik.errors.email && formik.touched.email && (
                <p className="m-0 mt-1 text-red-600">{formik.errors.email}</p>
              )}
              <div className="mt-4 mb-2">Họ và Tên:</div>
              <input
                type="text"
                name="fullname"
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập họ và tên..."
              />
              {formik.errors.fullname && formik.touched.fullname && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.fullname}
                </p>
              )}
              <div className="mt-4 mb-2">Mật khẩu:</div>
              <input
                type="text"
                name="password"
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập mật khẩu..."
              />
              {formik.errors.password && formik.touched.password && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.password}
                </p>
              )}
              <div className="mt-4 mb-2">Phân quyền:</div>
              <Select
                className="w-48"
                placeholder="Chọn phân quyền"
                name="role"
                onChange={changeSelect}
              >
                {["STAFF", "CLIENT"].map((user) => {
                  return <Select.Option value={user}>{user}</Select.Option>;
                })}
              </Select>
              {formik.errors.role && formik.touched.role && (
                <p className="m-0 mt-1 text-red-600">{formik.errors.role}</p>
              )}
              <div className="mt-4 mb-2">Số điện thoại:</div>
              <input
                type="text"
                name="phonenumber"
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập số điện thoại..."
              />
              {formik.errors.phonenumber && formik.touched.phonenumber && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.phonenumber}
                </p>
              )}
              <div className="mt-4 mb-2">Địa chỉ:</div>
              <input
                type="text"
                name="address"
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Nhập địa chỉ..."
              />
              {formik.errors.address && formik.touched.address && (
                <p className="m-0 mt-1 text-red-600">{formik.errors.address}</p>
              )}
              <div className="text-end mt-16">
                <button
                  type="submit"
                  className="border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
                >
                  Thêm
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
