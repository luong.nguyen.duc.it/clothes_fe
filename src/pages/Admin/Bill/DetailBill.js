import React, { Fragment, useEffect } from "react";
import { history } from "./../../../App";
import { _admin, _bill } from "../../../utils/Utils/ConfigPath";
import { BsBackspace } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { GetBillDetail } from "../../../redux/Actions/ManageCheckoutAction";
import { IMG } from "../../../utils/Setting/config";
import { Table } from "antd";
import { GetAllRankAction } from "../../../redux/Actions/ManageRankAction";

export default function DetailBill(props) {
  let { id } = props.match.params;

  const dispatch = useDispatch();

  const { lstBillDetail } = useSelector((state) => state.ManageCartReducer);
  const { lstRank } = useSelector((state) => state.ManageRankReducer);

  console.log("🚀 ~ DetailBill ~ lstBillDetail:", lstBillDetail);

  useEffect(() => {
    dispatch(GetBillDetail(id));
    dispatch(GetAllRankAction());
  }, []);

  let discountUser = 0;
  lstRank.map((item, index) => {
    if (item._id === lstBillDetail?.account?.rank) {
      discountUser = item.discount;
    }
  });

  const customPrice = (item) => {
    const discountedPrice = Math.floor(
      item.Price - item.Price * (item.Discount / 100)
    );

    const roundedDiscountedPrice =
      discountedPrice % 1000 >= 500
        ? Math.ceil(discountedPrice / 1000) * 1000
        : Math.floor(discountedPrice / 1000) * 1000;
    return roundedDiscountedPrice;
  };

  const columns = [
    {
      title: "Tên sản phẩm",
      dataIndex: "account",
      width: "20%",
      render: (text, item) => {
        return (
          <span className="text-base font-medium">{item.ProductName}</span>
        );
      },
    },
    {
      title: "Hình ảnh",
      dataIndex: "account",
      width: "12%",
      render: (text, item) => {
        return (
          <Fragment>
            <img
              className="h-36 w-36"
              src={`${IMG}${item.ProductImage}`}
              alt={`${item.ProductName}`}
            />
          </Fragment>
        );
      },
    },
    {
      title: "Mô tả",
      dataIndex: "account",
      render: (text, item) => {
        return (
          <Fragment>
            <span className="text-base font-medium">{item.Description}</span>
          </Fragment>
        );
      },
    },
    {
      title: "Số lượng",
      dataIndex: "account",
      width: "6%",
      render: (text, item) => {
        return (
          <Fragment>
            <span className="text-base font-medium">{item.Quantity}</span>
          </Fragment>
        );
      },
    },
    {
      title: "Giá tiền",
      dataIndex: "account",
      width: "10%",
      render: (text, item) => {
        return (
          <span className="text-base font-medium text-red-500">
            {(item.Price * 1).toLocaleString()}{" "}
            <span className="underline">đ</span>
          </span>
        );
      },
    },
    {
      title: "Giảm giá",
      dataIndex: "account",
      width: "10%",
      render: (text, item) => {
        return (
          <span className="text-base font-medium text-green-500">
            {item.Discount !== 0 ? <span>{item.Discount} %</span> : ""}
          </span>
        );
      },
    },
    {
      title: "Giảm giá tài khoản",
      dataIndex: "Discount",
      width: "10%",
      render: (text, item) => {
        console.log("item", item);
        return (
          <span className="text-base font-medium text-green-500">
            {discountUser} %
          </span>
        );
      },
    },
    {
      title: "Đã giảm giá",
      dataIndex: "Price",
      width: "10%",
      render: (text, item) => {
        const result =
          customPrice(item) - (customPrice(item) * discountUser) / 100;
        const roundedDiscountedPrice =
          result % 1000 >= 500
            ? Math.ceil(result / 1000) * 1000
            : Math.floor(result / 1000) * 1000;
        return (
          <span className="text-base font-medium text-red-500">
            {roundedDiscountedPrice.toLocaleString()}
            <span className="underline">đ</span>
          </span>
        );
      },
    },
  ];
  return (
    <Fragment>
      <div className="mt-4">
        <div>
          <button
            type="button"
            title="Trở về trang hóa đơn"
            className="text-4xl text-[#06283d] hover:text-green-900"
            onClick={() => {
              history.push(`${_admin}${_bill}`);
            }}
          >
            <BsBackspace />
          </button>
        </div>
        <h2 className="text-4xl font-bold text-center text-[#06283d]">
          Chi tiết hóa đơn
        </h2>
        <div className="my-10 flex justify-end"></div>
        <Table
          dataSource={lstBillDetail.checkout_detail}
          columns={columns}
          rowKey="_id"
        />
        ;
      </div>
    </Fragment>
  );
}
