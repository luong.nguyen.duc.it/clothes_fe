import { Checkbox, Input, Table } from "antd";
import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../../App";
import { _admin, _bill, _detail } from "../../../utils/Utils/ConfigPath";
import { AiFillHeart, AiOutlineMore } from "react-icons/ai";
import moment from "moment";
import {
  ChangeStatusAwaitAction,
  ChangeStatusDeliveryAction,
  ChangeStatusDoneAction,
  GetListBill,
} from "../../../redux/Actions/ManageCheckoutAction";
import { GetListUserAction } from "../../../redux/Actions/ManageAccountAction";

export default function ManageBill() {
  const { Search } = Input;
  const [discountUser, setDiscountUser] = useState(null);
  const dispatch = useDispatch();
  const { lstUser } = useSelector((state) => state.ManageAccountReducer);

  const { lstBill } = useSelector((state) => state.ManageCartReducer);
  console.log("🚀 ~ ManageBill ~ lstBill:", lstBill);
  useEffect(() => {
    dispatch(GetListBill());
  }, []);
  const onSearch = (value) => {
    console.log(value);
  };

  const handleChangeStatusAwait = (bill, value) => {
    const data = {
      id: bill._id,
      StatusAwait: value.checked,
    };
    dispatch(ChangeStatusAwaitAction(data));
  };
  const handleChangeDelivery = (bill, value) => {
    const data = {
      id: bill._id,
      StatusDelivery: value.checked,
    };

    dispatch(ChangeStatusDeliveryAction(data));
  };
  const handleChangeDone = (bill, value) => {
    const data = {
      id: bill._id,
      StatusDone: value.checked,
    };
    dispatch(ChangeStatusDoneAction(data));
  };

  const uppercaseToTitleCase = (inputText) => {
    if (!inputText) {
      return "";
    }

    const lowercaseText = inputText.toLowerCase();
    const titleCaseText = lowercaseText.replace(/(?:^|\s)\S/g, (match) => {
      return match.toUpperCase();
    });

    return titleCaseText;
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "_id",
      render: (text, record, index) => index + 1,
      width: "2%",
    },
    {
      title: "Tài khoản",
      dataIndex: "fullname",
      render: (text, cart) => {
        return <div>{uppercaseToTitleCase(cart.account.fullname)}</div>;
      },
    },
    {
      title: "Số điện thoại",
      dataIndex: "phonenumber",

      render: (text, cart) => {
        return <div>{cart.account.phonenumber}</div>;
      },
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      render: (text, cart) => {
        return <div>{cart.account.address}</div>;
      },
    },
    {
      title: "Tổng tiền",
      dataIndex: "total_money",
      render: (text, cart) => {
        // let discountUser = 0

        return (
          <span className="text-base font-medium text-red-500">
            {cart.total_money.toLocaleString()}{" "}
            <span className="underline">đ</span>
          </span>
        );
      },
    },
    {
      title: "Ngày thanh toán",
      dataIndex: "createdAt",
      render: (text, cart) => {
        return (
          <span className="text-base">
            {moment(cart.createdAt).format("DD/MM/YYYY")}
          </span>
        );
      },
    },
    {
      title: "Tình trạng đơn hàng",
      dataIndex: "StatusAwait",
      render: (text, cart) => {
        return (
          <div className="flex justify-center">
            {/* <Checkbox
                        onChange={(event) => {
                            handleChangeStatusAwait(event.target);
                        }}
                        style={{ transform: "scale(1.5)" }}
                    /> */}
            <div className="flex flex-col justify-center gap-2">
              <div className="flex justify-start gap-2">
                <Checkbox
                  checked={cart.status_await}
                  disabled={cart.status_delivery}
                  onChange={(event) => {
                    handleChangeStatusAwait(cart, event.target);
                  }}
                  style={{ transform: "scale(1.5) mr-4" }}
                />
                Done Chuẩn bị hàng
              </div>
              <div className="flex justify-start gap-2">
                <Checkbox
                  checked={cart.status_delivery}
                  disabled={cart.status_delivery}
                  onChange={(event) => {
                    handleChangeDelivery(cart, event.target);
                  }}
                  style={{ transform: "scale(1.5) mr-4" }}
                />
                Đang giao hàng
              </div>
              <div className="flex justify-start gap-2">
                {cart.status_delivery ? (
                  <Fragment>
                    <Checkbox
                      checked={cart.status_done}
                      disabled={cart.status_done}
                      onChange={(event) => {
                        handleChangeDone(cart, event.target);
                      }}
                      style={{ transform: "scale(1.5) mr-4" }}
                    />
                    Giao hàng thành công
                  </Fragment>
                ) : null}
              </div>
            </div>
          </div>
        );
      },
    },
    {
      title: "Trạng thái",
      dataIndex: "status_done",
      render: (text, cart) => {
        //! 1 done - 2 delivery  3- await
        let status;
        if (cart.status_done) {
          status = "Giao hàng thành công";
        } else if (!cart.status_done && cart.status_delivery) {
          status = "Đang vận chuyển";
        } else if (
          !cart.status_done &&
          !cart.status_delivery &&
          cart.status_await
        ) {
          status = "Đang chuẩn bị hàng";
        }
        return <span className="text-base text-green-500">{status}</span>;
      },
    },

    {
      title: "",
      dataIndex: "id",
      render: (text, item) => {
        return (
          <div className="flex">
            <button
              className="mx-4 text-green-500 hover:text-green-900"
              title="Xem chi tiết hóa đơn"
              onClick={() => {
                history.push(`${_admin}${_bill}${_detail}/${item._id}`);
                sessionStorage.setItem("discount", discountUser);
              }}
            >
              <AiOutlineMore style={{ fontSize: 30 }} />
            </button>
          </div>
        );
      },
      width: "10%",
    },
  ];
  return (
    <Fragment>
      <div className="container mt-4">
        <div className="flex justify-center">
          <h2 className="text-4xl font-bold text-[#06283d] flex items-center">
            Quản lý hóa đơn
          </h2>
        </div>
        <div className="my-10 flex justify-end">
          <div className="w-1/3">
            <Search
              size="large"
              placeholder="Bạn muốn tìm gì?..."
              onSearch={onSearch}
              enterButton
            />
          </div>
        </div>
        <Table dataSource={lstBill} columns={columns} rowKey="id" />;
      </div>
    </Fragment>
  );
}
