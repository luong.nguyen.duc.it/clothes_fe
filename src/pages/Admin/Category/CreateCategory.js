import { useFormik } from "formik";
import * as Yup from "yup";
import React, { Fragment, useState } from "react";
import { BsBackspace } from "react-icons/bs";
import { useDispatch } from "react-redux";
import { history } from "../../../App";
import { AddCateAction } from "../../../redux/Actions/ManageCategoryAction";
import { _admin, _cate } from "../../../utils/Utils/ConfigPath";

export default function CreateCategory() {
  const dispatch = useDispatch();

  const [img, setImg] = useState("");
  const handleChangeFile = (e) => {
    let file = e.target.files[0];
    if (
      file.type === "image/jpeg" ||
      file.type === "image/jpeg" ||
      file.type === "image/jpg" ||
      file.type === "image/png"
    ) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        setImg(e.target.result); //Hinh base 64
      };
      formik.setFieldValue("category", file);
    }
  };

  const formik = useFormik({
    initialValues: {
      CategoryName: "",
      category: {},
    },
    validationSchema: Yup.object({
      CategoryName: Yup.string().required("Không được trống !"),
    }),
    onSubmit: (values) => {
      let dataProduct = new FormData();
      for (let key in values) {
        if (key !== "category") {
          dataProduct.append(key, values[key]);
        } else {
          dataProduct.append("category", values.category, values.category.name);
        }
      }
      dispatch(AddCateAction(dataProduct));
    },
  });
  return (
    <Fragment>
      <div className="grid grid-cols-7">
        <div>
          <button
            type="button"
            title="Trở về trang loại sản phẩm"
            className="text-4xl text-[#06283d] hover:text-green-900"
            onClick={() => {
              history.push(`${_admin}${_cate}`);
            }}
          >
            <BsBackspace />
          </button>
        </div>
        <div className="col-span-3 col-start-3 mt-32 h-96 rounded-lg shadow-2xl bg-white p-4">
          <h1 className="text-center text-4xl font-bold text-[#06283d]">
            Thêm loại sản phẩm
          </h1>
          <div className="p-4">
            <form onSubmit={formik.handleSubmit}>
              <div className="mb-2">Tên loại sản phẩm:</div>
              <input
                type="text"
                name="CategoryName"
                onChange={formik.handleChange}
                className="p-3 border-gray border rounded-lg focus:outline-none focus:border-[#06283d] focus:ring-1 focus:ring-[#06283d] w-full"
                placeholder="Tên loại sản phẩm..."
              />
              {formik.errors.CategoryName && formik.touched.CategoryName && (
                <p className="m-0 mt-1 text-red-600">
                  {formik.errors.CategoryName}
                </p>
              )}
              <div className="my-4">
                <span className="mr-2">Hình ảnh:</span>
                <input
                  name="products"
                  type="file"
                  onChange={handleChangeFile}
                  accept="image/jpeg, image/jpg, image/png"
                />
              </div>
              <div className="my-4">
                <img className="w-36 h-36 rounded-md" src={img} alt="..." />
              </div>
              <div className="text-end mt-16">
                <button
                  type="submit"
                  className="border-2 border-[#06283d] rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
                >
                  Thêm{" "}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
