import { Input, Popconfirm, Table } from "antd";
import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../../App";
import { _add, _admin, _cate, _edit } from "../../../utils/Utils/ConfigPath";
import {
  DeleteCate,
  GetAllCateAction,
} from "./../../../redux/Actions/ManageCategoryAction";
import { BsFillTrashFill, BsPencilSquare } from "react-icons/bs";
import { IMG } from "../../../utils/Setting/config";

export default function ManageCategory() {
  const { Search } = Input;
  const onSearch = (value) => console.log(value);

  const dispatch = useDispatch();

  const { lstCate } = useSelector((state) => state.ManageCategoryReducer);

  useEffect(() => {
    dispatch(GetAllCateAction());
  }, []);
  const cancel = (e) => {
    console.log(e);
  };
  const columns = [
    {
      title: "STT",
      dataIndex: "_id",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Loại sản phẩm",
      dataIndex: "category_name",
    },
    {
      title: "Hình ảnh",
      dataIndex: "category_image",
      render: (text, item) => {
        return (
          <Fragment>
            <img
              className="h-36 w-36"
              src={`${IMG}${item.category_image}`}
              alt={item.category_image}
            />
          </Fragment>
        );
      },
      width: "12%",
    },
    {
      title: "",
      dataIndex: "id",
      render: (text, cate) => {
        return (
          <div className="flex">
            <button
              className="mx-4 text-green-500 hover:text-green-900"
              title="Sửa"
              onClick={() => {
                history.push(`${_admin}${_cate}${_edit}/${cate._id}`);
              }}
            >
              <BsPencilSquare style={{ fontSize: 25 }} />
            </button>
            <button
              className="mx-4 text-red-500 hover:text-red-900"
              title="Xóa"
            >
              <Popconfirm
                title="Bạn có chắc muốn xóa loại sản phẩm này không?"
                onConfirm={() => {
                  dispatch(DeleteCate(cate._id));
                }}
                onCancel={cancel}
                okText="Có"
                cancelText="Không"
              >
                <BsFillTrashFill style={{ fontSize: 25 }} />
              </Popconfirm>
            </button>
          </div>
        );
      },
    },
  ];

  return (
    <Fragment>
      <div className="container mt-4">
        <h2 className="text-4xl font-bold text-center text-[#06283d]">
          Quản lý loại sản phẩm
        </h2>
        <div className="my-10 flex justify-between">
          <button
            type="button"
            className="border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
            onClick={() => {
              history.push(`${_admin}${_cate}${_add}`);
            }}
          >
            Thêm{" "}
          </button>
          <div className="w-1/2">
            <Search
              size="large"
              placeholder="Bạn muốn tìm gì?..."
              onSearch={onSearch}
              enterButton
            />
          </div>
        </div>
        <Table dataSource={lstCate} columns={columns} rowKey="id" />;
      </div>
    </Fragment>
  );
}
