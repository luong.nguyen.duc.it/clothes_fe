import React, { Fragment, useEffect } from "react";
import { history } from "./../../../App";
import { Input, Popconfirm, Table } from "antd";
import { BsFillTrashFill, BsPencilSquare } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import {
  DeleteWorkScheduleAction,
  GetAllWorkScheduleAction,
} from "../../../redux/Actions/ManageWorkScheduleAction";
import moment from "moment";
import { GetListUserAction } from "../../../redux/Actions/ManageAccountAction";
import {
  _add,
  _admin,
  _edit,
  _workSchedule,
} from "../../../utils/Utils/ConfigPath";

export default function ManageWorkSchedule() {
  const dispatch = useDispatch();

  const { lstWork } = useSelector((state) => state.ManageWorkScheduleReducer);
  const { lstUser } = useSelector((state) => state.ManageAccountReducer);
  console.log("🚀 ~ ManageWorkSchedule ~ lstUser:", lstUser);

  const uppercaseToTitleCase = (inputText) => {
    if (!inputText) {
      return "";
    }

    const lowercaseText = inputText.toLowerCase();
    const titleCaseText = lowercaseText.replace(/(?:^|\s)\S/g, (match) => {
      return match.toUpperCase();
    });

    return titleCaseText;
  };

  useEffect(() => {
    dispatch(GetAllWorkScheduleAction());
    dispatch(GetListUserAction());
  }, []);
  const formatTime = (hours) => {
    const integerPart = Math.floor(hours);
    const decimalPart = (hours - integerPart) * 60;

    let formattedTime = `${integerPart}h`;

    // Kiểm tra nếu phần thập phân là 0 thì thêm số 0 vào
    if (decimalPart === 0) {
      formattedTime += "00";
    } else {
      formattedTime += decimalPart;
    }
    return formattedTime;
  };

  const cancel = (e) => {
    console.log(e);
  };

  const { Search } = Input;
  const columns = [
    {
      title: "STT",
      dataIndex: "id",
      width: "2%",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Tên ca làm",
      dataIndex: "workSchedule_name",
    },
    {
      title: "Ngày làm",
      dataIndex: "date",
      render: (text, item) => {
        return <Fragment>{moment(item.date).format("DD-MM-YYYY")}</Fragment>;
      },
    },
    {
      title: "Giờ bắt đầu ca",
      dataIndex: "from_time",
      render: (text, item) => {
        return <Fragment>{formatTime(item.from_time)}</Fragment>;
      },
    },
    {
      title: "Giờ kết thúc ca",
      dataIndex: "to_time",
      render: (text, item) => {
        return <Fragment>{formatTime(item.to_time)}</Fragment>;
      },
    },
    {
      title: "Nhân viên",
      dataIndex: "account",
      render: (text, item) => {
        return (
          <Fragment>
            {lstUser?.map((user, index) => {
              return (
                <span key={index}>
                  {user._id === item.account ? (
                    <span>{uppercaseToTitleCase(user.fullname)}</span>
                  ) : null}
                </span>
              );
            })}
          </Fragment>
        );
      },
    },
    {
      title: "Hệ số",
      dataIndex: "allowance_factor",
    },

    {
      title: "Tình trạng",
      dataIndex: "complete",
      render: (text, item) => {
        return (
          <Fragment>
            {item.complete ? (
              <span className="text-green-500">Hoàn thành</span>
            ) : (
              <span className="text-orange-500">Chưa hoàn thành</span>
            )}
          </Fragment>
        );
      },
    },
    {
      title: "Lý do",
      dataIndex: "reason",
    },
    {
      title: "",
      dataIndex: "id",
      render: (text, item) => {
        return (
          <div className="flex">
            <button
              className="mx-4 text-green-500 hover:text-green-900"
              title="Sửa"
              onClick={() => {
                history.push(`${_admin}${_workSchedule}${_edit}/${item._id}`);
              }}
            >
              <BsPencilSquare style={{ fontSize: 25 }} />
            </button>
            <button
              className="mx-4 text-red-500 hover:text-red-900"
              title="Xóa"
            >
              <Popconfirm
                title="Bạn có chắc muốn xóa sản phẩm không?"
                onConfirm={() => {
                  dispatch(DeleteWorkScheduleAction(item._id));
                }}
                onCancel={cancel}
                okText="Có"
                cancelText="Không"
              >
                <BsFillTrashFill style={{ fontSize: 25 }} />
              </Popconfirm>
            </button>
          </div>
        );
      },
      width: "5%",
    },
  ];
  return (
    <Fragment>
      <div className="mt-4">
        <div className="flex justify-center">
          <h2 className="text-4xl font-bold text-[#06283d] flex items-center">
            Quản lý Ca Làm
          </h2>
        </div>
        <div className="my-10 flex justify-between">
          <button
            type="button"
            className="border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-[#06283d] hover:text-white hover:bg-[#06283d]"
            onClick={() => {
              history.push(`${_admin}${_workSchedule}${_add}`);
            }}
          >
            Thêm{" "}
          </button>
          <div className="w-1/3">
            <Search
              size="large"
              placeholder="Bạn muốn tìm gì?..."
              onSearch=""
              enterButton
            />
          </div>
        </div>
        <Table dataSource={lstWork} columns={columns} rowKey="id" />;
      </div>
    </Fragment>
  );
}
