import React, { Fragment, useEffect, useState } from "react";
import { history } from "../../../App";
import {
  _admin,
  _product,
  _workSchedule,
} from "../../../utils/Utils/ConfigPath";
import { BsBackspace } from "react-icons/bs";
import { Input, Select, Switch, Space, DatePicker } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { GetAllCateAction } from "../../../redux/Actions/ManageCategoryAction";
import { useFormik } from "formik";
import * as Yup from "yup";
import { AddProductAction } from "./../../../redux/Actions/ManageProductAction";
import moment from "moment";
import { uppercaseToTitleCase } from "../../../utils/Setting/config";
import { GetListUserAction } from "../../../redux/Actions/ManageAccountAction";
import { AddWorkScheduleAction } from "../../../redux/Actions/ManageWorkScheduleAction";

export default function CreateWorkSchedule() {
  const { TextArea } = Input;

  const { lstUser } = useSelector((state) => state.ManageAccountReducer);
  const [lstStaff, setLstStaff] = useState([]);

  useEffect(() => {
    const filteredStaff = lstUser?.filter(
      (item) => item.role.role_type === "STAFF"
    );

    setLstStaff(filteredStaff || []);
  }, [lstUser]);

  useEffect(() => {
    dispatch(GetListUserAction());
  }, []);
  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      WorkScheduleName: "",
      Date: "",
      FromTime: "",
      ToTime: "",
      Account_ID: "",
      AllowanceFactor: "",
      Complete: "",
      Reason: "",
    },
    validationSchema: Yup.object({
      WorkScheduleName: Yup.string().required("Không được trống !"),

      Date: Yup.date().required("Không được trống !"),

      FromTime: Yup.string().required("Không được trống !"),

      ToTime: Yup.string().required("Không được trống !"),

      Account_ID: Yup.string().required("Không được trống !"),

      AllowanceFactor: Yup.string().required("Không được trống !"),
    }),
    onSubmit: (values) => {
      dispatch(AddWorkScheduleAction(values));
    },
  });

  const changeSelect = (value) => {
    formik.setFieldValue("Account_ID", value);
  };
  const handleDate = (value) => {
    console.log({ value });
    formik.setFieldValue(
      "Date",
      moment(value).format("YYYY-MM-DDTHH:mm:ss.SSSZ")
    );
  };

  return (
    <Fragment>
      <div>
        <div>
          <button
            type="button"
            title="Trở về trang ca làm"
            className="text-4xl text-[#06283d] hover:text-green-900"
            onClick={() => {
              history.push(`${_admin}${_workSchedule}`);
            }}
          >
            <BsBackspace />
          </button>
        </div>
        <h1 className="text-center text-4xl font-bold text-[#06283d]">
          Thêm ca làm
        </h1>
        <form onSubmit={formik.handleSubmit}>
          <div className="grid grid-cols-7">
            <div className="col-start-2 col-span-3 mr-2">
              <div className="my-4">
                <div>Tên ca làm:</div>
                <input
                  type="text"
                  name="WorkScheduleName"
                  onChange={formik.handleChange}
                  className="p-2 px-4 border w-2/3 rounded drop-shadow-lg hover:border-[#06283d] focus:outline-none focus:border focus:border-[#06283d]"
                  placeholder="Nhập tên ca làm..."
                />
                {formik.errors.WorkScheduleName &&
                  formik.touched.WorkScheduleName && (
                    <p className="m-0 mt-1 text-red-600">
                      {formik.errors.WorkScheduleName}
                    </p>
                  )}
              </div>
              <div className="my-4">
                <div>Ngày làm:</div>
                <Space direction="vertical" className="w-full" size={16}>
                  <DatePicker
                    name="Date"
                    placeholder="Chọn ngày..."
                    onChange={handleDate}
                    className="w-2/3 h-10 rounded drop-shadow-lg"
                    format={"DD/MM/YYYY"}
                  />
                </Space>
                {formik.errors.Date && formik.touched.Date && (
                  <p className="m-0 mt-1 text-red-600">{formik.errors.Date}</p>
                )}
              </div>
              <div className="my-4">
                <div>Giờ bắt đầu ca:</div>
                <input
                  type="text"
                  name="FromTime"
                  onChange={formik.handleChange}
                  className="p-2 px-4 border w-2/3 rounded drop-shadow-lg hover:border-[#06283d] focus:outline-none focus:border focus:border-[#06283d]"
                  placeholder="Nhập giờ bắt đầu..."
                />
                {formik.errors.FromTime && formik.touched.FromTime && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.FromTime}
                  </p>
                )}
              </div>
              <div className="my-4">
                <div>Giờ kết thúc ca:</div>
                <input
                  type="text"
                  name="ToTime"
                  onChange={formik.handleChange}
                  className="p-2 px-4 border w-2/3 rounded drop-shadow-lg hover:border-[#06283d] focus:outline-none focus:border focus:border-[#06283d]"
                  placeholder="Nhập giờ kết thúc..."
                />
                {formik.errors.ToTime && formik.touched.ToTime && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.ToTime}
                  </p>
                )}
              </div>
            </div>
            <div className="col-span-3">
              <div className="my-4">
                <div>Nhân viên:</div>
                <Select
                  className="w-2/3 shadow-lg"
                  placeholder="Chọn nhân viên..."
                  name="Account_ID"
                  size="large"
                  onChange={changeSelect}
                >
                  {lstStaff.map((item, index) => {
                    return (
                      <Select.Option key={index} value={item._id}>
                        {uppercaseToTitleCase(item.fullname)}
                      </Select.Option>
                    );
                  })}
                </Select>
                {formik.errors.Account_ID && formik.touched.Account_ID && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.Account_ID}
                  </p>
                )}
              </div>
              <div className="my-4">
                <div>Hệ số:</div>
                <input
                  type="text"
                  name="AllowanceFactor"
                  onChange={formik.handleChange}
                  className="p-2 px-4 border w-2/3 rounded drop-shadow-lg hover:border-[#06283d] focus:outline-none focus:border focus:border-[#06283d]"
                  placeholder="Nhập hệ số..."
                />
                {formik.errors.AllowanceFactor &&
                  formik.touched.AllowanceFactor && (
                    <p className="m-0 mt-1 text-red-600">
                      {formik.errors.AllowanceFactor}
                    </p>
                  )}
              </div>
              {/* <div className="my-4">
                <div>Tình trạng:</div>
                <input
                  type="number"
                  name="Quantity"
                  onChange={formik.handleChange}
                  className="p-2 px-4 border w-2/3 rounded drop-shadow-lg hover:border-[#06283d] focus:outline-none focus:border focus:border-[#06283d]"
                  placeholder="Số lượng..."
                />
                {formik.errors.Quantity && formik.touched.Quantity && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.Quantity}
                  </p>
                )}
              </div>
              <div className="my-4">
                <div>Lý do:</div>
                <input
                  type="number"
                  name="Quantity"
                  onChange={formik.handleChange}
                  className="p-2 px-4 border w-2/3 rounded drop-shadow-lg hover:border-[#06283d] focus:outline-none focus:border focus:border-[#06283d]"
                  placeholder="Số lượng..."
                />
                {formik.errors.Quantity && formik.touched.Quantity && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.Quantity}
                  </p>
                )}
              </div> */}
            </div>
          </div>
          <div className="text-center">
            <button
              type="submit"
              className="text-center p-3 border border-green-900 w-36 text-xl font-bold rounded text-[#06283d] hover:bg-[#06283d] hover:text-white"
            >
              Thêm
            </button>
          </div>
        </form>
      </div>
    </Fragment>
  );
}
