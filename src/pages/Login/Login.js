import { useFormik } from "formik";
import * as Yup from "yup";
import React, { Fragment } from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { _register } from "../../utils/Utils/ConfigPath";
import { _logo } from "../../utils/Utils/ImgPath";
import { LoginAction } from "../../redux/Actions/ManageAccountAction";

export default function Login() {
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Email không đúng!"),
      password: Yup.string()
        .min(6, "Tối thiểu 6 kí tự")
        .required("Không được trống !"),
    }),

    onSubmit: (values) => {
      dispatch(LoginAction(values));
    },
  });
  return (
    <Fragment>
      <div className="grid grid-cols-12 my-40">
        <div className="col-start-5 col-span-4">
          <div className="border p-4 rounded-md shadow-lg">
            <div className="uppercase mb-8 font-bold text-3xl flex justify-around items-center">
              <div>Đăng nhập</div>
              <img className="h-20" src={_logo} alt="Roway Offical" />
            </div>
            <form onSubmit={formik.handleSubmit}>
              <div className="relative z-0 mb-6 w-full group">
                <input
                  type="text"
                  name="email"
                  onChange={formik.handleChange}
                  className="block py-2.5 px-0 w-full text-lg text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-black dark:border-gray-600 dark:focus:border-[#06283d] focus:outline-none focus:ring-0 focus:border-[#06283d] peer"
                  placeholder=" "
                  required
                />
                {formik.errors.email && formik.touched.email && (
                  <p className="m-0 mt-1 text-red-600">{formik.errors.email}</p>
                )}
                <label
                  htmlFor="floating_first_name"
                  className="peer-focus:font-medium absolute text-lg text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-[#06283d] peer-focus:dark:text-[#06283d] peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Email
                </label>
              </div>

              <div className="relative z-0 mb-6 w-full group">
                <input
                  type="password"
                  name="password"
                  onChange={formik.handleChange}
                  className="block py-2.5 px-0 w-full text-lg text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-black dark:border-gray-600 dark:focus:border-[#06283d] focus:outline-none focus:ring-0 focus:border-[#06283d] peer"
                  placeholder=" "
                  required
                />
                {formik.errors.password && formik.touched.password && (
                  <p className="m-0 mt-1 text-red-600">
                    {formik.errors.password}
                  </p>
                )}
                <label
                  htmlFor="floating_password"
                  className="peer-focus:font-medium absolute text-lg text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-[#06283d] peer-focus:dark:text-[#06283d] peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Mật khẩu
                </label>
              </div>
              <button
                type="submit"
                className="text-white bg-[#06283d] focus:ring-4 focus:outline-none font-medium rounded-lg text-lg w-full lg:w-auto px-5 py-2.5 text-center "
              >
                Đăng nhập
              </button>
              <div className="mt-4">
                Bạn chưa có tài khoản?{" "}
                <NavLink
                  to={_register}
                  className="font-medium text-black hover:text-[#06283d] hover:underline"
                >
                  Đăng ký ngay
                </NavLink>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
