export const DOMAIN = "http://localhost:8090/api/v1";
export const IMG = "http://localhost:8090/";

export const uppercaseToTitleCase = (inputText) => {
  if (!inputText) {
    return "";
  }

  const lowercaseText = inputText.toLowerCase();
  const titleCaseText = lowercaseText.replace(/(?:^|\s)\S/g, (match) => {
    return match.toUpperCase();
  });

  return titleCaseText;
};
