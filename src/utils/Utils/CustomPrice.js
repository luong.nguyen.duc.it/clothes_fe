export const customPriceDetail = (item) => {
  const discountedPrice = Math.floor(
    item.price - item.price * (item.discount / 100)
  );

  const roundedDiscountedPrice =
    discountedPrice % 1000 >= 500
      ? Math.ceil(discountedPrice / 1000) * 1000
      : Math.floor(discountedPrice / 1000) * 1000;
  return roundedDiscountedPrice;
};
