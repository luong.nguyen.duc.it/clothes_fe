export const _home = "/";
export const _product = "/product";
export const _detail = "/detail";
export const _cart = "/cart";
export const _order = "/order";

export const _login = "/login";
export const _register = "/register";

export const _admin = "/admin";

export const _dashboard = "/dashboard";

export const _account = "/account";
export const _add = "/add";
export const _edit = "/edit";

export const _cate = "/category";
export const _categories = "/categories";

export const _rank = "/rank";
export const _role = "/role";

export const _bill = "/bill";

export const _intro = "/introduce";
export const _point = "/point";
export const _workSchedule = "/workSchedule";
export const _salary = "/salary";
