import { baseService } from "./baseService";

class ManageCheckoutService extends baseService {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  checkout = (data) => {
    return this.post("checkout", data);
  };

  requireCheckout = (data) => {
    return this.post("checkout/RequireCheckout/paypal", data);
  };
  checkoutWithYear = (data) => {
    return this.post(`checkout/Total/Chart`, { year: data });
  };

  changeStatusAwait = (data) => {
    return this.put("checkout/ChangeStatusAwait", data);
  };
  changeStatusDelivery = (data) => {
    return this.put("checkout/ChangeStatusDelivery", data);
  };
  changeStatusDone = (data) => {
    return this.put("checkout/ChangeStatusDone", data);
  };

  getBillDetail = (id) => {
    return this.get(`checkout/Detail/${id}`);
  };

  getCheckout = (Id) => {
    return this.get(`checkout/ByAccount/${Id}`);
  };

  getListBill = () => {
    return this.get(`checkout`);
  };
}

export const manageCheckoutService = new ManageCheckoutService();
