import { baseService } from "./baseService";

class ManageProductService extends baseService {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  addProduct = (dataProduct) => {
    return this.post(`product`, dataProduct);
  };

  getAll = () => {
    return this.get(`product/all`);
  };
  getDetail = (id) => {
    return this.get(`product/${id}`);
  };

  getProductByName = (data) => {
    return this.get(`product?ProductName=${data}`);
  };

  updateCheckRead = (id) => {
    return this.put(`product/check/${id}`);
  };

  getAllProductHot = () => {
    return this.get(`checkout/product`);
  };

  getProductByCate = (id) => {
    return this.get(`product/categories/${id}`);
  };

  updateProduct = (id, data) => {
    return this.put(`product/${id}`, data);
  };

  delProduct = (id) => {
    return this.delete(`product/${id}`);
  };
}

export const manageProductService = new ManageProductService();
