import { baseService } from "./baseService";

class ManageReviewService extends baseService {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  addReview = (data) => {
    return this.post(`review`, data);
  };

  getAllReview = (id) => {
    return this.get(`review/byProduct/${id}`);
  };

  getStar = (id) => {
    return this.get(`review/byId/${id}`);
  };
  updateRank = (id, data) => {
    return this.put(`review/${id}`, data);
  };
  delRank = (id) => {
    return this.delete(`review/${id}`);
  };
}

export const manageReviewService = new ManageReviewService();
