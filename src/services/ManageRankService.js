import { baseService } from "./baseService";

class ManageRankService extends baseService {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  addRank = (data) => {
    return this.post(`ranks`, data);
  };

  getAllRank = () => {
    return this.get(`ranks`);
  };

  getDetailRank = (id) => {
    return this.get(`ranks/${id}`);
  };
  updateRank = (id, data) => {
    return this.put(`ranks/${id}`, data);
  };
  delRank = (id) => {
    return this.delete(`ranks/${id}`);
  };
}

export const manageRankService = new ManageRankService();
