import { baseService } from "./baseService";

class ManageAccountService extends baseService {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  signUp = (dataSignUp) => {
    return this.post(`accounts/signup`, dataSignUp);
  };

  signIn = (dataSignIn) => {
    return this.post(`accounts/signin`, dataSignIn);
  };

  getAll = () => {
    return this.get(`accounts`);
  };

  getDetail = (id) => {
    return this.get(`accounts/${id}`);
  };

  updateAccount = (id, data) => {
    return this.put(`accounts/${id}`, data);
  };

  updateAccountPoint = (id, point) => {
    return this.put(`accounts/Point/${id}`, point);
  };
  delUser = (id) => {
    return this.delete(`accounts/${id}`);
  };
}

export const manageAccountService = new ManageAccountService();
