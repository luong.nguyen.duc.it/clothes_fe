import { baseService } from "./baseService";

class ManageRoleService extends baseService {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  addRole = (data) => {
    return this.post(`roles`, data);
  };

  getAllRole = () => {
    return this.get(`roles`);
  };

  getDetailRole = (id) => {
    return this.get(`roles/${id}`);
  };
  updateRole = (id, data) => {
    return this.put(`roles/${id}`, data);
  };
  delRole = (id) => {
    return this.delete(`roles/${id}`);
  };
}

export const manageRoleService = new ManageRoleService();
