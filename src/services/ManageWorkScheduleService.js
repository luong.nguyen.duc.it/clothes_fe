import { baseService } from "./baseService";

class ManageWorkScheduleService extends baseService {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  addWork = (data) => {
    return this.post(`workSchedule`, data);
  };

  getAllWork = () => {
    return this.get(`workSchedule`);
  };

  getDetailWork = (id) => {
    return this.get(`workSchedule/${id}`);
  };
  updateWork = (id, data) => {
    return this.put(`workSchedule/${id}`, data);
  };
  getTotalWorkScheduleByAccountAndMonth = (data) => {
    return this.put(`workSchedule/total`, data);
  };
  delWork = (id) => {
    return this.delete(`workSchedule/${id}`);
  };
}

export const manageWorkScheduleService = new ManageWorkScheduleService();
