import { baseService } from "./baseService";

class ManageCategoryService extends baseService {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  addCate = (data) => {
    return this.post(`category`, data);
  };

  getAllCate = () => {
    return this.get(`category`);
  };

  getDetailCate = (id) => {
    return this.get(`category/${id}`);
  };
  updateCate = (id, data) => {
    return this.put(`category/${id}`, data);
  };
  delCate = (id) => {
    return this.delete(`category/${id}`);
  };
}

export const manageCategoryService = new ManageCategoryService();
