import React from "react";
import { createBrowserHistory } from "history/cjs/history";
import { Switch, Router } from "react-router";
import HomeTemplate from "./template/HomeTemplate/HomeTemplate";
import {
  _account,
  _add,
  _admin,
  _bill,
  _cart,
  _cate,
  _categories,
  _dashboard,
  _detail,
  _edit,
  _home,
  _login,
  _order,
  _point,
  _product,
  _rank,
  _register,
  _role,
  _salary,
  _workSchedule,
} from "./utils/Utils/ConfigPath";
import Login from "./pages/Login/Login";
import Home from "./pages/Client/Home/Home";
import AdminTemplate from "./template/AdminTemplate/AdminTemplate";
import Cart from "./pages/Client/Cart/Cart";
import ProductList from "./pages/Client/Product/ProductList";
import ProductDetail from "./pages/Client/Product/ProductDetail";
import UpdateAccount from "./pages/Client/Account/UpdateAccount";
import OrderHistory from "./pages/Client/Account/OrderHistory";
import Register from "./pages/Register/Register";
import ManageAccount from "./pages/Admin/Account/ManageAccount";
import ManageCategory from "./pages/Admin/Category/ManageCategory";
import ManageProduct from "./pages/Admin/Product/ManageProduct";
import ManageBill from "./pages/Admin/Bill/ManageBill";
import CreateAccount from "./pages/Admin/Account/CreateAccount";
import CreateCategory from "./pages/Admin/Category/CreateCategory";
import CreateProduct from "./pages/Admin/Product/CreateProduct";
import UpdateProduct from "./pages/Admin/Product/UpdateProduct";
import DetailBill from "./pages/Admin/Bill/DetailBill";
import UpdateCategory from "./pages/Admin/Category/UpdateCategory";
import PaymentSuccess from "./component/Payment/PaymentSuccess";
import PaymentError from "./component/Payment/PaymentError";
import { Route } from "react-router-dom/cjs/react-router-dom.min";
import ManageWorkSchedule from "./pages/Admin/WorkSchedule/ManageWorkSchedule";
import ManageSalary from "./pages/Admin/Salary/ManageSalary";
import AdminUpdateAccount from "./pages/Admin/Account/UpdateAccount";
import CreateWorkSchedule from "./pages/Admin/WorkSchedule/CreateWorkSchedule";
import UpdateWorkSchedule from "./pages/Admin/WorkSchedule/UpdateWorkSchedule";
import ManageRank from "./pages/Admin/Rank/ManageRank";
import UpdateRank from "./pages/Admin/Rank/UpdateRank";
import CreateRank from "./pages/Admin/Rank/CreateRank";
import ManageRole from "./pages/Admin/Role/ManageRole";
import CreateRole from "./pages/Admin/Role/CreateRole";
import UpdateRole from "./pages/Admin/Role/UpdateRole";
import Dashboard from "./pages/Admin/Dashboard/Dashboard";
import Point from "./pages/Client/Introduce/Point";
import AllProduct from "./pages/Client/Product/AllProduct";

export const history = createBrowserHistory();

export default function App() {
  return (
    <Router history={history}>
      <Switch>
        <HomeTemplate path={_home} exact Component={Home} />
        <HomeTemplate path={_cart} exact Component={Cart} />
        <HomeTemplate
          path={`${_categories}/:id`}
          exact
          Component={ProductList}
        />
        <HomeTemplate path={`${_detail}/:id`} exact Component={ProductDetail} />
        <HomeTemplate
          path={`${_account}${_edit}`}
          exact
          Component={UpdateAccount}
        />
        <HomeTemplate path={`${_order}/:id`} exact Component={OrderHistory} />

        <Route path="/success" exact component={PaymentSuccess} />
        <Route path="/error" exact component={PaymentError} />

        <HomeTemplate path={_login} exact Component={Login} />
        <HomeTemplate path={_register} exact Component={Register} />
        <HomeTemplate path={_point} exact Component={Point} />
        <HomeTemplate path={_categories} exact Component={AllProduct} />

        <AdminTemplate
          path={`${_admin}${_dashboard}`}
          exact
          Component={Dashboard}
        />
        <AdminTemplate
          path={`${_admin}${_account}`}
          exact
          Component={ManageAccount}
        />
        <AdminTemplate
          path={`${_admin}${_account}${_add}`}
          exact
          Component={CreateAccount}
        />
        <AdminTemplate
          path={`${_admin}${_account}${_edit}/:id`}
          exact
          Component={AdminUpdateAccount}
        />

        <AdminTemplate
          path={`${_admin}${_workSchedule}`}
          exact
          Component={ManageWorkSchedule}
        />
        <AdminTemplate
          path={`${_admin}${_workSchedule}${_add}`}
          exact
          Component={CreateWorkSchedule}
        />
        <AdminTemplate
          path={`${_admin}${_workSchedule}${_edit}/:id`}
          exact
          Component={UpdateWorkSchedule}
        />
        <AdminTemplate
          path={`${_admin}${_salary}`}
          exact
          Component={ManageSalary}
        />

        <AdminTemplate
          path={`${_admin}${_cate}`}
          exact
          Component={ManageCategory}
        />
        <AdminTemplate
          path={`${_admin}${_cate}${_add}`}
          exact
          Component={CreateCategory}
        />
        <AdminTemplate
          path={`${_admin}${_cate}${_edit}/:id`}
          exact
          Component={UpdateCategory}
        />

        <AdminTemplate
          path={`${_admin}${_rank}`}
          exact
          Component={ManageRank}
        />
        <AdminTemplate
          path={`${_admin}${_rank}${_add}`}
          exact
          Component={CreateRank}
        />
        <AdminTemplate
          path={`${_admin}${_rank}${_edit}/:id`}
          exact
          Component={UpdateRank}
        />

        <AdminTemplate
          path={`${_admin}${_role}`}
          exact
          Component={ManageRole}
        />
        <AdminTemplate
          path={`${_admin}${_role}${_add}`}
          exact
          Component={CreateRole}
        />
        <AdminTemplate
          path={`${_admin}${_role}${_edit}/:id`}
          exact
          Component={UpdateRole}
        />

        <AdminTemplate
          path={`${_admin}${_product}`}
          exact
          Component={ManageProduct}
        />
        <AdminTemplate
          path={`${_admin}${_product}${_add}`}
          exact
          Component={CreateProduct}
        />
        <AdminTemplate
          path={`${_admin}${_product}${_edit}/:id`}
          exact
          Component={UpdateProduct}
        />

        <AdminTemplate
          path={`${_admin}${_bill}`}
          exact
          Component={ManageBill}
        />
        <AdminTemplate
          path={`${_admin}${_bill}${_detail}/:id`}
          exact
          Component={DetailBill}
        />
      </Switch>
    </Router>
  );
}
