import React, { Fragment, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { Dropdown, Menu, Space } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { GetAllCateAction } from "../../../redux/Actions/ManageCategoryAction";
import { history } from "../../../App";
import { _cart } from "../../../utils/Utils/ConfigPath";
import { _logo } from "../../../utils/Utils/ImgPath";
import { BsBasket } from "react-icons/bs";

export default function NavHeader() {
  const [parsedCart, setParsedCart] = useState(null);

  const cartFromSession = sessionStorage.getItem("cart");

  useEffect(() => {
    if (cartFromSession) {
      const parsedCart = JSON.parse(cartFromSession);
      setParsedCart(parsedCart);
    }
  }, [cartFromSession]);
  let number = 0;
  parsedCart?.forEach((element) => {
    number += element.Quantity;
  });
  const [scrollPosition, setScrollPosition] = useState(0);
  const [isScrolled, setIsScrolled] = useState(false);
  let timeoutId;
  useEffect(() => {
    const handleScroll = () => {
      clearTimeout(timeoutId);
      // eslint-disable-next-line react-hooks/exhaustive-deps
      timeoutId = setTimeout(() => {
        setScrollPosition(window.scrollY);
        setIsScrolled(window.scrollY > 40);
      }, 10);
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const headerClass = isScrolled ? "fixed top-0" : "";
  return (
    <div
      className={`w-full bg-white grid grid-cols-12 z-50 drop-shadow-md ${headerClass}`}
    >
      <div className="col-start-3 col-span-8 flex justify-between items-center">
        <NavLink to={"/"}>
          <img className="h-16" src={_logo} alt="Roway Offical" />
        </NavLink>
        <div>
          <NavLink
            to={`${_cart}`}
            className="ml-4 flex text-black text-2xl hover:text-[#06283d]"
          >
            <BsBasket />
            <span className="text-base text-red-500 -mt-1">({number})</span>
          </NavLink>
        </div>
      </div>
    </div>
  );
}
