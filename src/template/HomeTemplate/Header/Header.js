import React, { Fragment, useState, useEffect } from "react";
import { _logo } from "../../../utils/Utils/ImgPath";
import { BsBasket, BsPersonCircle, BsSearch } from "react-icons/bs";
import { NavLink } from "react-router-dom";
import NavHeader from "./NavHeader";
import {
  _account,
  _cart,
  _categories,
  _edit,
  _home,
  _login,
  _order,
  _register,
} from "../../../utils/Utils/ConfigPath";
import { useSelector } from "react-redux";
import { Dropdown, Menu, Space } from "antd";
import { history } from "../../../App";
import { USER_LOGIN } from "../../../redux/Types/ManageAccountType";
import _ from "lodash";
import { AiOutlinePhone } from "react-icons/ai";

export default function Header() {
  const [parsedCart, setParsedCart] = useState(null);
  const [search, setSearch] = useState();
  console.log("🚀 ~ Header ~ search:", search);
  const { userLogin } = useSelector((state) => state.ManageAccountReducer);

  const { cart } = useSelector((state) => state.ManageCartReducer);
  const valueSearch = sessionStorage.getItem("search");
  const cartFromSession = sessionStorage.getItem("cart");

  useEffect(() => {
    if (cartFromSession) {
      const parsedCart = JSON.parse(cartFromSession);
      setParsedCart(parsedCart);
    }
  }, [cartFromSession]);
  let number = 0;
  parsedCart?.forEach((element) => {
    number += element.Quantity;
  });
  const handleSearch = () => {
    if (search === undefined) {
      sessionStorage.setItem("search", "");
    } else {
      sessionStorage.setItem("search", search);
    }
  };
  const menu = (
    <Menu
      items={[
        {
          label: (
            <button
              onClick={() => {
                history.push(`${_account}${_edit}`);
                window.location.reload();
              }}
              className="self-center px-4 py-2 hover:font-bold hover:text-green-900"
            >
              Thông tin tài khoản
            </button>
          ),
          key: "0",
        },
        {
          label: (
            <Fragment>
              <button
                onClick={() => {
                  history.push(`${_order}/${userLogin.accountLogin._id}`);
                  window.location.reload();
                }}
                className="self-center px-4 py-2 hover:font-bold hover:text-green-900"
              >
                Lịch sử đặt hàng
              </button>
            </Fragment>
          ),
          key: "1",
        },
        {
          label: (
            <button
              onClick={() => {
                sessionStorage.removeItem(USER_LOGIN);
                sessionStorage.removeItem("TOKEN");
                sessionStorage.removeItem("cart");
                history.push(`${_home}`);
                window.location.reload();
              }}
              className="self-center px-4 py-2 hover:font-bold hover:text-green-900"
            >
              Đăng xuất
            </button>
          ),
          key: "2",
        },
      ]}
    />
  );

  const operations = (
    <Fragment>
      {!_.isEmpty(userLogin) ? (
        <Dropdown overlay={menu} trigger={["click"]}>
          <span onClick={(e) => e.preventDefault()}>
            <Space>
              <div className="flex items-center text-black mt-2 cursor-pointer hover:text-green-900">
                <BsPersonCircle className="text-2xl mr-1" />
                <span className="flex items-center text-base font-bold ">
                  {userLogin?.accountLogin?.fullname}
                </span>
              </div>
            </Space>
          </span>
        </Dropdown>
      ) : (
        <Fragment>
          <NavLink
            to={_login}
            className="border text-black py-2 px-4 mx-2 rounded-lg hover:text-green-900 hover:border-green-900 "
          >
            Đăng nhập
          </NavLink>
          <NavLink
            to={_register}
            className="border text-black py-2 px-4 mx-2 rounded-lg hover:text-green-900  hover:border-green-900 "
          >
            Đăng ký
          </NavLink>
        </Fragment>
      )}
    </Fragment>
  );
  return (
    <div className="w-full z-50 bg-white">
      <div className="grid grid-cols-12 mt-2">
        <div className="col-start-3 col-span-8">
          <div className="grid grid-cols-8 ">
            <div className="col-span-2 flex items-center font-bold">
              <AiOutlinePhone className="text-lg" />
              Hotline: 0337074546
            </div>
            <div className="col-span-3">
              <div className="relative w-full ">
                <input
                  type="text"
                  // value={valueSearch}
                  className="border text-gray-900 text-sm rounded-lg focus:outline-none focus:ring focus:ring-green-900 focus:shadow-lg block w-full pl-4 p-2.5 "
                  placeholder="Tìm kiếm danh mục, thương hiệu hoặc các sản phẩm..."
                  onChange={(value) => setSearch(value.target.value)}
                />
                <NavLink
                  to={_categories}
                  className="absolute inset-y-0 right-4 flex items-center pl-3"
                  onClick={() => handleSearch()}
                >
                  <BsSearch className="text-green-900 text-lg" />
                </NavLink>
              </div>
            </div>
            <div className="col-span-3 flex justify-end items-center">
              <div className="flex items-center uppercase">
                {operations}
                {/* <NavLink
                  to={`${_cart}`}
                  className="ml-4 flex text-black text-2xl hover:text-green-900"
                >
                  <BsBasket />
                  <span className="text-base text-red-500 -mt-1">
                    ({number})
                  </span>
                </NavLink> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <NavHeader />
    </div>
  );
}
