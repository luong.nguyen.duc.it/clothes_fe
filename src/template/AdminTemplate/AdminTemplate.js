import { Layout, Menu, message } from "antd";
import React, { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { USER_LOGIN } from "../../redux/Types/ManageAccountType";
import {
  _account,
  _admin,
  _bill,
  _cate,
  _dashboard,
  _home,
  _login,
  _product,
  _rank,
  _role,
  _salary,
  _workSchedule,
} from "../../utils/Utils/ConfigPath";
import { Redirect, Route } from "react-router";
import { NavLink } from "react-router-dom";
import { history } from "../../App";
import _ from "lodash";
import { _logo } from "../../utils/Utils/ImgPath";

const { Content, Sider } = Layout;

export default function AdminTemplate(props) {
  const { Component, ...restRoute } = props;
  const [collapsed, setCollapsed] = useState(false);
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  const { userLogin } = useSelector((state) => state.ManageAccountReducer);
  const role = userLogin?.accountLogin.role.role_type;
  if (!sessionStorage.getItem(USER_LOGIN)) {
    message.error("Bạn chưa đăng nhập!");
    return <Redirect to={_login} />;
  }
  if (userLogin.accountLogin.role.role_type === "CLIENT") {
    message.error("Bạn không có quyền truy cập vào trang này!");
    return <Redirect to={_home} />;
  }

  const operations = (
    <Fragment>
      {!_.isEmpty(userLogin) ? (
        <div className="flex">
          <NavLink to="/profile" className="flex">
            <div className="rounded-full border-2 shadow-sm">
              <img className="w-14 h-14 rounded-full " src={_logo} alt="" />
            </div>
            <span className="flex items-center border-r-2 border-green-900 text-lg font-bold mx-4 pr-4 cursor-pointer text-black">
              Xin chào!, {userLogin.accountLogin.fullname}
            </span>
          </NavLink>
          <button
            onClick={() => {
              sessionStorage.removeItem(USER_LOGIN);
              sessionStorage.removeItem("TOKEN");
              history.push(`${_home}`);
              window.location.reload();
            }}
            className="self-center mr-8 px-4 py-3 border border-green-900 rounded-lg text-[#06283d] text-lg font-bold hover:text-white hover:bg-[#06283d]"
          >
            Đăng xuất
          </button>
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );
  return (
    <Route
      {...restRoute}
      render={(propsRoute) => {
        return (
          <Fragment>
            <Layout style={{ minHeight: "100vh" }}>
              <Sider
                className="border-r"
                collapsible
                collapsed={collapsed}
                onCollapse={(value) => setCollapsed(value)}
                style={{ backgroundColor: "#000" }}
              >
                <div
                  onClick={() => {
                    history.push(`${_home}`);
                  }}
                  className="w-full flex justify-center my-4 cursor-pointer"
                  title="Trang chủ"
                >
                  <img className="h-20" src={_logo} alt="Phượt bụi Store" />
                </div>
                <Menu
                  theme="dark"
                  defaultSelectedKeys={role === "ADMIN" ? "1" : "5"}
                  mode="inline"
                  style={{ backgroundColor: "transparent" }}
                >
                  {role === "ADMIN" ? (
                    <>
                      <Menu.Item key="1">
                        <NavLink
                          className="text-black nav-link"
                          to={`${_admin}${_dashboard}`}
                        >
                          Dashboard
                        </NavLink>
                      </Menu.Item>
                      <Menu.Item key="2">
                        <NavLink
                          className="text-black nav-link"
                          to={`${_admin}${_account}`}
                        >
                          Accounts
                        </NavLink>
                      </Menu.Item>
                      <Menu.Item key="3">
                        <NavLink
                          className="text-black nav-link"
                          to={`${_admin}${_role}`}
                        >
                          Roles
                        </NavLink>
                      </Menu.Item>
                      <Menu.Item key="4">
                        <NavLink
                          className="text-black nav-link"
                          to={`${_admin}${_rank}`}
                        >
                          Ranks
                        </NavLink>
                      </Menu.Item>
                    </>
                  ) : null}

                  <Menu.Item key="5">
                    <NavLink
                      className="text-black nav-link"
                      to={`${_admin}${_workSchedule}`}
                    >
                      WorkSchedule
                    </NavLink>
                  </Menu.Item>
                  <Menu.Item key="6">
                    <NavLink
                      className="text-black nav-link"
                      to={`${_admin}${_salary}`}
                    >
                      Salary
                    </NavLink>
                  </Menu.Item>
                  <Menu.Item key="7">
                    <NavLink
                      className="text-black nav-link"
                      to={`${_admin}${_cate}`}
                    >
                      Category
                    </NavLink>
                  </Menu.Item>
                  <Menu.Item key="8">
                    <NavLink
                      className="text-black nav-link"
                      to={`${_admin}${_product}`}
                    >
                      Product
                    </NavLink>
                  </Menu.Item>
                  <Menu.Item key="9">
                    <NavLink
                      className="text-black nav-link"
                      to={`${_admin}${_bill}`}
                    >
                      Bill
                    </NavLink>
                  </Menu.Item>
                </Menu>
              </Sider>
              <Layout className="site-layout">
                <div className="bg-white border-b shadow-lg">
                  <div className="flex justify-end mt-1">{operations}</div>
                </div>
                <Content style={{ margin: "0 16px" }}>
                  <div style={{ padding: 24, minHeight: 360 }}>
                    <Component {...propsRoute} />
                  </div>
                </Content>
                <div className="py-4 text-center border-t-2">
                  Created by Luong
                </div>
              </Layout>
            </Layout>
          </Fragment>
        );
      }}
    />
  );
}
