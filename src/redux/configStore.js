import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";

import { ManageAccountReducer } from "./Reducers/ManageAccountReducer";
import { ManageCategoryReducer } from "./Reducers/ManageCategoryReducer";
import { ManageProductReducer } from "./Reducers/ManageProductReducer";
import { ManageCartReducer } from "./Reducers/ManageCartReducer";
import { ManageWorkScheduleReducer } from "./Reducers/ManageWorkScheduleReducer";
import { ManageRankReducer } from "./Reducers/ManageRankReducer";
import { ManageRoleReducer } from "./Reducers/ManageRoleReducer";
import { ManageReviewReducer } from "./Reducers/ManageReviewReducer";

const rootReducers = combineReducers({
  ManageAccountReducer,
  ManageCategoryReducer,
  ManageProductReducer,
  ManageCartReducer,
  ManageWorkScheduleReducer,
  ManageRankReducer,
  ManageRoleReducer,
  ManageReviewReducer,
});

export const store = createStore(rootReducers, applyMiddleware(thunk));
