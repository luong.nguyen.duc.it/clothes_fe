import { GET_ALL_REVIEW_PRODUCT, GET_STAR } from "../Types/ManageReviewType";

const initialState = {
  dataStar: [],
  lstReview: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export const ManageReviewReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_STAR: {
      state.dataStar = action.dataStar;
      return { ...state };
    }

    case GET_ALL_REVIEW_PRODUCT: {
      state.lstReview = action.dataReview;
      return { ...state };
    }

    default:
      return state;
  }
};
