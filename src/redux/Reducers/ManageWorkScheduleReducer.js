import {
  GET_ALL_WORK_SCHEDULE,
  GET_DETAIL_WORK,
  GET_TOTAL_MONEY,
} from "../Types/ManageWorkScheduleType";

const initialState = {
  lstWork: [],
  detailWork: [],
  detailTotal: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export const ManageWorkScheduleReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_WORK_SCHEDULE: {
      state.lstWork = action.dataWork;
      return { ...state };
    }
    case GET_DETAIL_WORK: {
      state.detailWork = action.dataDetail;
      return { ...state };
    }
    case GET_TOTAL_MONEY: {
      state.detailTotal = action.dataTotal;
      return { ...state };
    }
    default:
      return state;
  }
};
