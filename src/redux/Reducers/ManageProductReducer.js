import {
  GET_ALL_PRODUCT,
  GET_ALL_PRODUCT_HOT,
  GET_DETAIL_PRODUCT,
  GET_PRODUCT_BY_CATE,
} from "../Types/ManageProductType";

const initialState = {
  lstProduct: [],
  detailProduct: [],
  lstProductByCate: [],
  lstProductHot: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export const ManageProductReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_PRODUCT: {
      state.lstProduct = action.dataProduct;
      return { ...state };
    }
    case GET_DETAIL_PRODUCT: {
      state.detailProduct = action.dataDetail;
      return { ...state };
    }

    case GET_PRODUCT_BY_CATE: {
      state.lstProductByCate = action.dataProductByCate;
      return { ...state };
    }

    case GET_ALL_PRODUCT_HOT: {
      state.lstProductHot = action.dataProductHot;
      return { ...state };
    }

    default:
      return state;
  }
};
