import {
  GET_ALL_USER,
  GET_DETAIL,
  SET_LOGIN,
  USER_LOGIN,
  POINT,
} from "../Types/ManageAccountType";

let userDefault = {};
if (sessionStorage.getItem(USER_LOGIN)) {
  userDefault = JSON.parse(sessionStorage.getItem(USER_LOGIN));
}

const initialState = {
  userLogin: userDefault,
  lstUser: [],
  editUser: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export const ManageAccountReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN: {
      const { dataSignIn } = action;
      sessionStorage.setItem(USER_LOGIN, JSON.stringify(dataSignIn));

      return { ...state, userLogin: dataSignIn };
    }
    case GET_ALL_USER: {
      state.lstUser = action.dataUser;
      return { ...state };
    }
    case GET_DETAIL: {
      state.editUser = action.dataDetail;
      return { ...state };
    }
    case POINT: {
      let listUser = action.data.lstUser;
      // console.log('lst', listUser)
      let point = Math.floor(action.data.total * 0.0001);
      let userLogin = JSON.parse(sessionStorage.getItem(USER_LOGIN));
      listUser.map((item, index) => {
        if (item.id === userLogin.account.id) {
          item.Point += point;
        }
      });
      // if (userLogin) {
      //     userLogin.account.Point += point;
      //     sessionStorage.setItem(USER_LOGIN, JSON.stringify(userLogin));
      // }
      // console.log('userLogin', userLogin)
      return { ...state };
    }

    default:
      return state;
  }
};
