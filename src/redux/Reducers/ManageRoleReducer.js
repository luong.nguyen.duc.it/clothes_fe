import { GET_ALL_ROLE, GET_DETAIL_ROLE } from "../Types/ManageRoleType";

const initialState = {
  lstRole: [],
  detailRole: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export const ManageRoleReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_ROLE: {
      state.lstRole = action.dataRole;
      return { ...state };
    }
    case GET_DETAIL_ROLE: {
      state.detailRole = action.dataDetail;
      return { ...state };
    }

    default:
      return state;
  }
};
