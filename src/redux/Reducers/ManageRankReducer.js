import { GET_ALL_RANK, GET_DETAIL_RANK } from "../Types/ManageRankType";

const initialState = {
  lstRank: [],
  detailRank: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export const ManageRankReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_RANK: {
      state.lstRank = action.dataRank;
      return { ...state };
    }
    case GET_DETAIL_RANK: {
      state.detailRank = action.dataDetail;
      return { ...state };
    }

    default:
      return state;
  }
};
