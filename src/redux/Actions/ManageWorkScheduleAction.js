import { manageWorkScheduleService } from "../../services/ManageWorkScheduleService";
import { message } from "antd";
import {
  GET_ALL_WORK_SCHEDULE,
  GET_DETAIL_WORK,
  GET_TOTAL_MONEY,
} from "../Types/ManageWorkScheduleType";
import { _admin, _workSchedule } from "../../utils/Utils/ConfigPath";
import { history } from "../../App";

export const AddWorkScheduleAction = (dataWork) => {
  return async (dispatch) => {
    try {
      const result = await manageWorkScheduleService.addWork(dataWork);
      if (result.status === 200) {
        history.push(`${_admin}${_workSchedule}`);
        await message.success("Thêm mới thành công!");
      } else {
        message.error("Thêm mới thất bại!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const GetAllWorkScheduleAction = () => {
  return async (dispatch) => {
    try {
      const result = await manageWorkScheduleService.getAllWork();
      if (result.status === 200) {
        dispatch({
          type: GET_ALL_WORK_SCHEDULE,
          dataWork: result.data,
        });
      } else {
        message.error("Không lấy được ca làm!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};
export const GetDetailWorkScheduleAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageWorkScheduleService.getDetailWork(id);
      if (result.status === 200) {
        dispatch({
          type: GET_DETAIL_WORK,
          dataDetail: result.data,
        });
      } else {
        message.warning("error!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const TotalWorkScheduleByAccountAndMonth = (data) => {
  return async (dispatch) => {
    try {
      const result =
        await manageWorkScheduleService.getTotalWorkScheduleByAccountAndMonth(
          data
        );
      if (result.status === 200) {
        dispatch({
          type: GET_TOTAL_MONEY,
          dataTotal: result.data,
        });
      } else {
        message.warning("error!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const UpdateWorkScheduleAction = (id, data) => {
  return async (dispatch) => {
    try {
      const result = await manageWorkScheduleService.updateWork(id, data);
      if (result.status === 200) {
        await message.success("Cập nhật ca làm thành công!");
        history.push(`${_admin}${_workSchedule}`);
      } else {
        message.error("Cập nhật ca làm thất bại!");
      }
    } catch (error) {
      message.error("Cập nhật ca làm thất bại!!");
      console.log("error", error.response?.data);
    }
  };
};

export const DeleteWorkScheduleAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageWorkScheduleService.delWork(id);
      if (result.status === 200) {
        message.success("Xóa thành công!");
        dispatch(GetAllWorkScheduleAction());
      } else {
        message.warning("Xóa thất bại!");
      }
    } catch (error) {
      message.warning("Xóa thất bại!");
      console.log("error", error.response?.data);
    }
  };
};
