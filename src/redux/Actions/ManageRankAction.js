import { message } from "antd";
import { history } from "../../App";
import { _admin, _rank } from "../../utils/Utils/ConfigPath";
import { manageRankService } from "../../services/ManageRankService";
import { GET_ALL_RANK, GET_DETAIL_RANK } from "../Types/ManageRankType";

export const AddRankAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageRankService.addRank(data);
      if (result.status === 201) {
        await message.success("Thêm mới thành công!");
        history.push(`${_admin}${_rank}`);
      } else {
        message.error("Thêm mới thất bại!");
      }
    } catch (error) {
      message.error("Loại cấp độ này đã tồn tại!");
      console.log("error", error.response?.data);
    }
  };
};

export const GetAllRankAction = () => {
  return async (dispatch) => {
    try {
      const result = await manageRankService.getAllRank();
      if (result.status === 200) {
        dispatch({
          type: GET_ALL_RANK,
          dataRank: result.data,
        });
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const GetDetailRankAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageRankService.getDetailRank(id);
      if (result.status === 200) {
        dispatch({
          type: GET_DETAIL_RANK,
          dataDetail: result.data,
        });
      } else {
        message.warning("error!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const UpdateRankAction = (id, data) => {
  return async (dispatch) => {
    try {
      const result = await manageRankService.updateRank(id, data);
      if (result.status === 200) {
        await message.success("Cập nhật cấp độ thành công!");
        history.push(`${_admin}${_rank}`);
      } else {
        message.error("Cập nhật cấp độ thất bại!");
      }
    } catch (error) {
      message.error("Cập nhật cấp độ thất bại!!");
      console.log("error", error.response?.data);
    }
  };
};

export const DeleteRank = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageRankService.delRank(id);
      if (result.status === 200) {
        message.success("Xóa thành công!");
        dispatch(GetAllRankAction());
      } else {
        message.warning("Xóa thất bại!");
      }
    } catch (error) {
      message.warning("Xóa thất bại!");
      console.log("error", error.response?.data);
    }
  };
};
