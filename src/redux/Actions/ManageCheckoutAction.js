import {
  CLEAR_CART,
  GET_BILL_DETAIL,
  GET_CHART,
  GET_CHECKOUT_HISTORY,
  GET_LIST_BILL,
} from "../Types/ManageCartType";
import { manageCheckoutService } from "./../../services/ManageCheckoutService";
import { history } from "./../../App";
import { _home, _login } from "../../utils/Utils/ConfigPath";
import { message } from "antd";
import { USER_LOGIN } from "../Types/ManageAccountType";

export const RequirementCheckoutAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageCheckoutService.requireCheckout(data);
      if (result.status === 200) {
        window.open(result.data, "_blank").focus();
      }
    } catch (error) {
      console.log(error);
    }
  };
};

export const ChangeStatusAwaitAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageCheckoutService.changeStatusAwait(data);
      if (result.status === 200) {
        dispatch(GetListBill());
      }
    } catch (error) {
      message.error("ERROR");
    }
  };
};
export const ChangeStatusDeliveryAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageCheckoutService.changeStatusDelivery(data);
      if (result.status === 200) {
        dispatch(GetListBill());
      }
    } catch (error) {
      message.error("ERROR");
    }
  };
};

export const ChangeStatusDoneAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageCheckoutService.changeStatusDone(data);
      if (result.status === 200) {
        dispatch(GetListBill());
      }
    } catch (error) {
      message.error("ERROR");
    }
  };
};
export const CheckoutAction = (data) => {
  return async (dispatch) => {
    if (JSON.parse(sessionStorage.getItem(USER_LOGIN))) {
      const id = JSON.parse(sessionStorage.getItem(USER_LOGIN)).accountLogin
        ._id;
      try {
        const dataCheckout = {
          Account_ID: id,
          ListProduct: data,
        };
        const result = await manageCheckoutService.checkout(dataCheckout);
        // if (result.status === 200) {
        //     dispatch({
        //         type: CLEAR_CART
        //     })
        //     // alert("Thanh toán thành công!")
        //     // history.push(`${_home}`)
        //     // window.location.href = _home;
        // }
        // else {
        //     alert('Thanh toán không thành công!')

        // }
      } catch (error) {
        console.log("error", error.response?.data);
        message.warning("Thanh toán không thành công!");
      }
    } else {
      alert("Bạn phải đăng nhập trước!");
      history.push(`${_login}`);
    }
  };
};

export const GetListBill = () => {
  return async (dispatch) => {
    try {
      const result = await manageCheckoutService.getListBill();
      if (result.status === 200) {
        dispatch({
          type: GET_LIST_BILL,
          data: result.data,
        });
      }
    } catch (error) {
      console.log("error", error.response?.data);
      message.warning("Lấy thông tin không thành công!");
    }
  };
};

export const GetBillDetail = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageCheckoutService.getBillDetail(id);
      if (result.status === 200) {
        dispatch({
          type: GET_BILL_DETAIL,
          data: result.data,
        });
      }
    } catch (error) {
      console.log("error", error.response?.data);
      message.warning("Lấy thông tin không thành công!");
    }
  };
};

export const GetCheckoutHistory = () => {
  return async (dispatch) => {
    const id = JSON.parse(sessionStorage.getItem("USER_LOGIN")).accountLogin
      ._id;
    try {
      const result = await manageCheckoutService.getCheckout(id);
      if (result.status === 200) {
        dispatch({
          type: GET_CHECKOUT_HISTORY,
          data: result.data,
        });
      }
    } catch (error) {
      console.log("error", error.response?.data);
      message.warning("Lấy thông tin không thành công!");
    }
  };
};

export const GetChartWithYear = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageCheckoutService.checkoutWithYear(data);
      if (result.status === 200) {
        dispatch({
          type: GET_CHART,
          data: result.data,
        });
      }
    } catch (error) {
      console.log("error", error.response?.data);
      message.warning("Lấy thông tin không thành công!");
    }
  };
};
