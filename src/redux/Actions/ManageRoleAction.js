import { message } from "antd";
import { history } from "../../App";
import { _admin, _rank, _role } from "../../utils/Utils/ConfigPath";
import { manageRoleService } from "../../services/ManageRoleService";
import { GET_ALL_ROLE, GET_DETAIL_ROLE } from "./../Types/ManageRoleType";

export const AddRoleAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageRoleService.addRole(data);
      if (result.status === 201) {
        await message.success("Thêm mới thành công!");
        history.push(`${_admin}${_role}`);
      } else {
        message.error("Thêm mới thất bại!");
      }
    } catch (error) {
      message.error("Loại cấp này đã tồn tại!");
      console.log("error", error.response?.data);
    }
  };
};

export const GetAllRoleAction = () => {
  return async (dispatch) => {
    try {
      const result = await manageRoleService.getAllRole();
      if (result.status === 200) {
        dispatch({
          type: GET_ALL_ROLE,
          dataRole: result.data,
        });
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const GetDetailRoleAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageRoleService.getDetailRole(id);
      if (result.status === 200) {
        dispatch({
          type: GET_DETAIL_ROLE,
          dataDetail: result.data,
        });
      } else {
        message.warning("error!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const UpdateRoleAction = (id, data) => {
  return async (dispatch) => {
    try {
      const result = await manageRoleService.updateRole(id, data);
      if (result.status === 200) {
        await message.success("Cập nhật cấp độ thành công!");
        history.push(`${_admin}${_rank}`);
      } else {
        message.error("Cập nhật cấp độ thất bại!");
      }
    } catch (error) {
      message.error("Cập nhật cấp độ thất bại!!");
      console.log("error", error.response?.data);
    }
  };
};

export const DeleteRole = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageRoleService.delRole(id);
      if (result.status === 200) {
        message.success("Xóa thành công!");
        dispatch(GetAllRoleAction());
      } else {
        message.warning("Xóa thất bại!");
      }
    } catch (error) {
      message.warning("Xóa thất bại!");
      console.log("error", error.response?.data);
    }
  };
};
