import { manageReviewService } from "../../services/ManageReviewService";
import { GET_ALL_REVIEW_PRODUCT, GET_STAR } from "../Types/ManageReviewType";
import { message } from "antd";

export const AddReviewAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageReviewService.addReview(data);
      console.log("🚀 ~ return ~ result:", result);
    } catch (error) {
      message.error("Loại sản phẩm này đã tồn tại!");
      console.log("error", error.response?.data);
    }
  };
};

export const GetStarReviewAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageReviewService.getStar(id);
      if (result.status === 200) {
        dispatch({
          type: GET_STAR,
          dataStar: result.data,
        });
      } else {
        message.warning("error!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const GetAllReviewAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageReviewService.getAllReview(id);
      if (result.status === 200) {
        dispatch({
          type: GET_ALL_REVIEW_PRODUCT,
          dataReview: result.data,
        });
      } else {
        message.warning("error!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};
