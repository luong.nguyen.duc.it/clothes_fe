import { message } from "antd";
import { manageProductService } from "../../services/ManageProductService";
import { _admin, _product } from "../../utils/Utils/ConfigPath";
import {
  GET_ALL_PRODUCT,
  GET_ALL_PRODUCT_HOT,
  GET_DETAIL_PRODUCT,
  GET_PRODUCT_BY_CATE,
} from "../Types/ManageProductType";
import { history } from "./../../App";

export const AddProductAction = (dataProduct) => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.addProduct(dataProduct);
      if (result.status === 200) {
        history.push(`${_admin}${_product}`);
        await message.success("Thêm mới thành công!");
      } else {
        message.error("Thêm mới thất bại!");
      }
    } catch (error) {
      message.error("Loại sản phẩm này đã tồn tại!");
      console.log("error", error.response?.data);
    }
  };
};

export const GetAllProductAction = () => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.getAll();
      if (result.status === 200) {
        dispatch({
          type: GET_ALL_PRODUCT,
          dataProduct: result.data,
        });
      } else {
        message.error("Không lấy được sản phẩm!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};
export const GetProductByNameAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.getProductByName(data);
      if (result.status === 200) {
        dispatch({
          type: GET_ALL_PRODUCT,
          dataProduct: result.data,
        });
      } else {
        message.error("Không lấy được sản phẩm!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const GetAllProductByCateAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.getProductByCate(id);
      if (result.status === 200) {
        dispatch({
          type: GET_PRODUCT_BY_CATE,
          dataProductByCate: result.data,
        });
      } else {
        message.error("Không lấy được sản phẩm!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const GetDetailProductAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.getDetail(id);
      if (result.status === 200) {
        dispatch({
          type: GET_DETAIL_PRODUCT,
          dataDetail: result.data,
        });
      } else {
        message.warning("error!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const UpdateCheckReadAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.updateCheckRead(id);
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const GetProductHot = () => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.getAllProductHot();
      if (result.status === 200) {
        dispatch({
          type: GET_ALL_PRODUCT_HOT,
          dataProductHot: result.data,
        });
      } else {
        message.warning("error!");
      }
    } catch (error) {
      console.log("error", error.response?.data);
    }
  };
};

export const UpdateProductAction = (id, data) => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.updateProduct(id, data);
      if (result.status === 200) {
        await message.success("Cập nhật sản phẩm thành công!");
        history.push(`${_admin}${_product}`);
      } else {
        message.error("Cập nhật sản phẩm thất bại!");
      }
    } catch (error) {
      message.error("Cập nhật sản phẩm thất bại!!");
      console.log("error", error.response?.data);
    }
  };
};

export const DeleteProductAction = (id) => {
  return async (dispatch) => {
    try {
      const result = await manageProductService.delProduct(id);
      if (result.status === 200) {
        message.success("Xóa thành công!");
        dispatch(GetAllProductAction());
      } else {
        message.warning("Xóa thất bại!");
      }
    } catch (error) {
      message.warning("Xóa thất bại!");
      console.log("error", error.response?.data);
    }
  };
};
