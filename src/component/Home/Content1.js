import React, { Fragment, useEffect } from "react";
import { darkBlue } from "../../utils/Utils/CustomColor";
import { useDispatch, useSelector } from "react-redux";
import { GetAllCateAction } from "../../redux/Actions/ManageCategoryAction";
import { IMG } from "../../utils/Setting/config";
import { NavLink } from "react-router-dom/cjs/react-router-dom.min";
import { _categories } from "../../utils/Utils/ConfigPath";

export default function Content1() {
  const dispatch = useDispatch();

  const { lstCate } = useSelector((state) => state.ManageCategoryReducer);

  useEffect(() => {
    dispatch(GetAllCateAction());
  }, []);
  return (
    <Fragment>
      <div className="text-center my-8">
        <div className={`uppercase text-3xl font-bold ${darkBlue}`}>
          Danh mục nổi bật
        </div>
        <div className="my-4 flex justify-around mx-24">
          {lstCate?.map((item, index) => {
            return (
              <div className="h-40 w-40">
                <NavLink to={`${_categories}/${item._id}`}>
                  <img src={`${IMG}${item.category_image}`} alt="" />
                  <div className="text-[#06283d] text-lg font-bold py-2 border-x border-b">
                    {item.category_name}
                  </div>
                </NavLink>
              </div>
            );
          })}
        </div>
      </div>
    </Fragment>
  );
}
