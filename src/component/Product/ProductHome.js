import React, { Fragment, useState } from "react";
import { _mu1 } from "../../utils/Utils/ImgPath";
import OneProduct from "./OneProduct";
import { darkBlue } from "../../utils/Utils/CustomColor";

export default function ProductHome(props) {
  const { product, title } = props;
  const renderProduct = () => {
    return product?.map((item, index) => {
      return <OneProduct key={index} product={item} />;
    });
  };

  return (
    <Fragment>
      <div className="text-center">
        <div className="grid grid-cols-3 items-center">
          <hr className="border-[1px] border-[#06283d] ml-10" />
          <div className={`uppercase text-4xl font-bold ${darkBlue}`}>
            {title}
          </div>
          <hr className="border-[1px] border-[#06283d] mr-10" />
        </div>

        <div className="flex mt-5 mx-20 overflow-x-auto whitespace-nowrap">
          {renderProduct()}
        </div>
      </div>
    </Fragment>
  );
}
