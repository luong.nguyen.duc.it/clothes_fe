import React from "react";
import { NavLink, useHistory } from "react-router-dom";
import { IMG } from "../../utils/Setting/config";
import _ from "lodash";
import { useDispatch } from "react-redux";
import { _detail } from "./../../utils/Utils/ConfigPath";
import { UpdateCheckReadAction } from "../../redux/Actions/ManageProductAction";

export default function OneProduct(props) {
  const { product } = props;
  const dispatch = useDispatch();

  const handleCheckRead = (id) => {
    dispatch(UpdateCheckReadAction(id));
  };
  const discountedPrice = Math.floor(
    product.price - (product.price * product.discount) / 100
  );

  const roundedDiscountedPrice =
    discountedPrice % 1000 >= 500
      ? Math.ceil(discountedPrice / 1000) * 1000
      : Math.floor(discountedPrice / 1000) * 1000;
  return (
    <div className="mx-2 mb-4 border shadow-lg">
      <div className="relative">
        <NavLink
          to={`${_detail}/${product._id}`}
          title={`${product.product_name}`}
          onClick={() => handleCheckRead(product?._id)}
        >
          <img
            className="hover:scale-105 h-52 w-52"
            src={`${IMG}${product.product_image}`}
            alt={`${product.product_name}`}
          />
        </NavLink>

        {product.discount > 0 ? (
          <div className="absolute top-2 left-2 w-10 h-10 flex items-center justify-center rounded-full bg-[#F13112] text-white">
            {product.discount}%
          </div>
        ) : (
          ""
        )}
      </div>

      <NavLink
        to={`${_detail}/${product._id}`}
        title={product.product_name}
        onClick={() => handleCheckRead(product?._id)}
      >
        <h3 className="mx-4 text-[#06283d] hover:text-green-900 mt-2 text-center">
          {product.product_name}
        </h3>
      </NavLink>
      {product.discount > 0 ? (
        <div className="flex justify-around mb-2">
          <div className="line-through">
            {(product.price * 1).toLocaleString()}
            <span className="underline">đ</span>
          </div>
          <div className="font-medium text-red-500">
            {roundedDiscountedPrice.toLocaleString()}
            <span className="underline">đ</span>
          </div>
        </div>
      ) : (
        <div className="font-medium text-red-500">
          {(product.price * 1).toLocaleString()}
          <span className="underline">đ</span>
        </div>
      )}
    </div>
  );
}
